﻿occitan = {
	color = occitan
	
	ethos = ethos_courtly
	heritage = heritage_langue_doc
	language = language_occitano_romance
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_poetry
		tradition_chivalry
		tradition_mendicant_mystics
		tradition_religious_patronage
	}
	
	name_list = name_list_occitan

	coa_gfx = { occitan_coa_gfx frankish_group_coa_gfx western_coa_gfx } 
	building_gfx = { mediterranean_building_gfx } 
	clothing_gfx = { western_clothing_gfx } 
	unit_gfx = { western_unit_gfx } 	

	ethnicities = {
		10 = caucasian_blond
		5 = caucasian_ginger
		45 = caucasian_brown_hair
		35 = caucasian_dark_hair
	}	
}

catalan = {
	color = { 0.76 0.42 0.08 }

	ethos = ethos_egalitarian
	heritage = heritage_langue_doc
	language = language_occitano_romance
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_equitable
		tradition_republican_legacy
		tradition_parochialism
		tradition_seafaring
	}

	dlc_tradition = {
		trait = tradition_fp2_ritualised_friendship
		requires_dlc_flag = the_fate_of_iberia
	}
	
	name_list = name_list_catalan
	
	coa_gfx = { iberian_group_coa_gfx western_coa_gfx }
	building_gfx = { iberian_building_gfx }
	clothing_gfx = { iberian_christian_clothing_gfx western_clothing_gfx }
	unit_gfx = { iberian_christian_unit_gfx }
	
	ethnicities = {
		10 = mediterranean
	}
}

pyrenaic = {
	color = { 252 221 9 }
	
	ethos = ethos_courtly
	heritage = heritage_langue_doc
	language = language_occitano_romance
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_equitable
		tradition_republican_legacy
		tradition_parochialism
		tradition_seafaring
	}
	
	name_list = name_list_french

	coa_gfx = { occitan_coa_gfx frankish_group_coa_gfx western_coa_gfx } 
	building_gfx = { mediterranean_building_gfx } 
	clothing_gfx = { western_clothing_gfx } 
	unit_gfx = { western_unit_gfx } 	

	ethnicities = {
		10 = caucasian_blond
		5 = caucasian_ginger
		45 = caucasian_brown_hair
		35 = caucasian_dark_hair
	}	
}
