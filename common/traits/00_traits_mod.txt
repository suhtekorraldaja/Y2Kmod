﻿# Standard Values
@pos_compat_high = 30
@pos_compat_medium = 15
@pos_compat_low = 5

@neg_compat_high = -30
@neg_compat_medium = -15
@neg_compat_low = -5

# TECH OUTCOMES
education_tech_1 = {
	minimum_age = 16
	intrigue = 2
	category = education
	monthly_intrigue_lifestyle_xp_gain_mult = 0.1
	
	ruler_designer_cost = 0
	
	culture_modifier = {
		parameter = poorly_educated_leaders_distrusted
		feudal_government_opinion = -10
	}
	
	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_education_tech_1_desc
			}
			desc = trait_education_tech_1_character_desc
		}
	}

	group = education_tech
	level = 1
}
education_tech_2 = {
	minimum_age = 16
	intrigue = 4
	category = education
	monthly_intrigue_lifestyle_xp_gain_mult = 0.2
	
	ruler_designer_cost = 20
	
	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_education_tech_2_desc
			}
			desc = trait_education_tech_2_character_desc
		}
	}

	group = education_tech
	level = 2
}
education_tech_3 = {
	minimum_age = 16
	intrigue = 6
	category = education
	monthly_intrigue_lifestyle_xp_gain_mult = 0.3
	
	ruler_designer_cost = 40
	
	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_education_tech_3_desc
			}
			desc = trait_education_tech_3_character_desc
		}
	}

	group = education_tech
	level = 3
}
education_tech_4 = {
	minimum_age = 16
	intrigue = 8
	category = education
	monthly_intrigue_lifestyle_xp_gain_mult = 0.4
	
	ruler_designer_cost = 80
	
	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_education_tech_4_desc
			}
			desc = trait_education_tech_4_character_desc
		}
	}

	group = education_tech
	level = 4
}
orc = {
	
	inherit_chance = 100 
	genetic = yes
	physical = yes
	orc_list = {
	birth = 100
	random_creation = 100
	}
	
	general_opinion = -50
	same_opinion = 50
	prowess = 4
	martial = 2
	diplomacy = -4
	learning = -2
	dread_baseline_add = 10
	
	ai_compassion = -20
	ai_boldness = 15
	ai_vengefulness = 10
	ai_war_chance = 20
	ai_war_cooldown = -20
	ai_honor = -15
	
	ruler_designer_cost = 0
	
	potential = {
	has_cultural_pillar = heritage_orcish
	}

	desc = {
		first_valid = {
			triggered_desc = {
				trigger = {
					NOT = { exists = this }
				}
				desc = trait_orc_desc
			}
			desc = trait_orc_character_desc
		}
	}
}

