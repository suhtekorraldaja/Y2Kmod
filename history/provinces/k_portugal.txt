##d_porto ###################################
###c_porto
1760 = {	# PORTO
	# Misc
	culture = galician
	religion = bruxaria
	
	holding = castle_holding
}
1761 = {	# BRAGA
	holding = city_holding
}
1762 = {	# CHAVEZ/Chaves
	holding = church_holding
}
1766 = {	# GUIMARAES
	holding = castle_holding
}

###c_braganza
1763 = {	# BRAGANCA
	# Misc
	culture = galician
	religion = bruxaria
	
	holding = castle_holding
}
1764 = {	# MIRANDA DO DUORO
	holding = city_holding
}
1765 = {	# VILLA REAL
	holding = church_holding
}
1767 = {	# MOGADOURO
	holding = none
}

##d_coimbra ###################################
###c_coimbra
1758 = {	# COIMBRA
	# Misc
	culture = portuguese
	religion = palmar
	
	holding = castle_holding
}
1757 = {	# LEIRIA
	holding = church_holding
}
1756 = {	# OBIDOS
	holding = city_holding
}

###c_viseu
1769 = {	# VISEU
	# Misc
	culture = portuguese
	religion = palmar
	
	holding = castle_holding
}
1770 = {	# TRANCOSO
	holding = city_holding
}

###c_aveiro
1759 = {	# 1759 - AVEIRO
	# Misc
	culture = portuguese
	religion = palmar
	
	holding = castle_holding
}
2003 = {	# GRALHEIRA/Cinfaes
	holding = none
}
1768 = {	# 1768 - LAMEGO
	holding = church_holding
}

###c_castelo_branco
1773 = {	# CASTELO BRANCO
	# Misc
	culture = portuguese
	religion = palmar
	
	holding = castle_holding
}
1771 = {	# IDANHA/Beira
	holding = city_holding
}
1772 = {	# ABRANTES
	holding = church_holding
}
