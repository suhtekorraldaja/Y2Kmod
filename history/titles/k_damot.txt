
d_damot = {
	867.1.1 = {	change_development_level = 3 }
	1066.1.1 = { change_development_level = 6 }

	763.1.1 = {
		liege = 0
	 145970
	government = tribal_government
	}
	798.1.1 = {
		 145971
	}
	823.1.1 = {
		 145972
	}
	862.1.1 = {
		 183506
	}
	935.1.1 = {
		government = feudal_government	#Ahistorical, just for ease of bookmarks.
	}
	1059.1.1 = {
	#liege = "k_abyssinia"
	 145736
	}
	1097.1.1 = {
		 145737
	}
	1125.1.1 = {
		 145738
	}
	1147.1.1 = {
		 145739
	}
	1200.1.1 = {
		 145740
	}
	1221.1.1 = {
		 145741
	}
	1258.1.1 = {
		 145742
	}
	1282.1.1 = {
		 145743
	}
	1320.1.1 = {
		 145744
	}
	1333.1.1 = {
		 145745
	}
}


c_damot = {
	763.1.1 = {
		liege = "d_damot"
	 145970
	government = tribal_government
	}
	798.1.1 = {
		 145971
	}
	823.1.1 = {
		 145972
	}
	862.1.1 = {
		 183506
	}
	935.1.1 = {
		government = feudal_government	#Ahistorical, just for ease of bookmarks.
	}
	1059.1.1 = {
		 145736
	}
	1097.1.1 = {
		 145737
	}
	1125.1.1 = {
		 145738
	}
	1147.1.1 = {
		 145739
	}
	1200.1.1 = {
		 145740
	}
	1221.1.1 = {
		 145741
	}
	1258.1.1 = {
		 145742
	}
	1282.1.1 = {
		 145743
	}
	1320.1.1 = {
		 145744
	}
	1333.1.1 = {
		 145745
	}
}


c_berta = {
	867.1.1 = {	change_development_level = 1 }
	1066.1.1 = { change_development_level = 3 }

	860.1.1 = {
		liege = "d_damot"
		 welayta0061
		government = tribal_government
	}
	890.1.1 = {
		 welayta0062
	}
	933.1.1 = {
		 welayta0063
	}
	990.1.1 = {
		 welayta0064
	}
	1050.1.1 = {
		 welayta0065
	}
	1081.1.1 = {
		 welayta0066
	}
	1120.1.1 = {
		 welayta0067
	}
	1159.1.1 = {
		 welayta0068
	}
	1199.1.1 = {
		 welayta0069
	}
	1235.1.1 = {
		 welayta0070
	}
}


c_wellega = {
	867.1.1 = {	change_development_level = 1 }
	1066.1.1 = { change_development_level = 3 }

	860.1.1 = {
		 welayta0071
		government = tribal_government
	}
	898.1.1 = {
		 welayta0072
	}
	950.1.1 = {
		 welayta0073
	}
	1000.1.1 = {
		 welayta0074
	}
	1035.1.1 = {
		 welayta0075
	}
	1065.1.1 = {
		liege = "d_damot"
	}
	1070.1.1 = {
		 welayta0076
	}
	1098.1.1 = {
		 welayta0077
	}
	1133.1.1 = {
		 welayta0078
	}
	1163.1.1 = {
		 welayta0079
	}
	1195.1.1 = {
		 welayta0080
	}
}


c_innarya = {
	763.1.1 = {
		liege = "d_damot"
	 145970
	government = tribal_government
	}
	798.1.1 = {
		 145971
	}
	823.1.1 = {
		 145972
	}
	862.1.1 = {
		 183506
	}
	935.1.1 = {
		government = feudal_government	#Ahistorical, just for ease of bookmarks.
	}
	1059.1.1 = {
		 145736
	}
	1097.1.1 = {
		 145737
	}
	1125.1.1 = {
		 145738
	}
	1147.1.1 = {
		 145739
	}
	1200.1.1 = {
		 145740
	}
	1221.1.1 = {
		 145741
	}
	1258.1.1 = {
		 145742
	}
	1282.1.1 = {
		 145743
	}
	1320.1.1 = {
		 145744
	}
	1333.1.1 = {
		 145745
	}
}


c_kaffa = {
	867.1.1 = {	change_development_level = 2 }
	1066.1.1 = { change_development_level = 4 }

	860.1.1 = {
		 welayta0071
		government = tribal_government
	}
	898.1.1 = {
		 welayta0072
	}
	950.1.1 = {
		 welayta0073
	}
	1000.1.1 = {
		 welayta0074
	}
	1035.1.1 = {
		 welayta0075
	}
	1065.1.1 = {
		liege = "d_damot"
	}
	1070.1.1 = {
		 welayta0076
	}
	1098.1.1 = {
		 welayta0077
	}
	1133.1.1 = {
		 welayta0078
	}
	1163.1.1 = {
		 welayta0079
	}
	1195.1.1 = {
		 welayta0080
	}
}


c_welamo = {
	867.1.1 = {	change_development_level = 2 }
	1066.1.1 = { change_development_level = 4 }

	850.1.1 = {
		government = tribal_government
		 welayta0041
	}
	893.1.1 = {
		 welayta0042
	}
	918.1.1 = {
		 welayta0043
	}
	968.1.1 = {
		 welayta0044
	}
	1025.1.1 = {
		 welayta0045
	}
	1066.1.1 = {
		 welayta0046
	}
	1097.1.1 = {
		 welayta0047
	}
	1176.1.1 = {
		 welayta0048
	}
	1173.1.1 = {
		 welayta0049
	}
	1210.1.1 = {
		 welayta0050
	}
}


c_gamo = {
	867.1.1 = {	change_development_level = 1 }
	1066.1.1 = { change_development_level = 3 }

	860.1.1 = {
		government = tribal_government
		 welayta0041
	}
	893.1.1 = {
		 welayta0042
	}
	918.1.1 = {
		 welayta0043
	}
	968.1.1 = {
		 welayta0044
	}
	1025.1.1 = {
		 welayta0045
	}
	1066.1.1 = {
		 welayta0046
	}
	1097.1.1 = {
		 welayta0047
	}
	1176.1.1 = {
		 welayta0048
	}
	1173.1.1 = {
		 welayta0049
	}
	1210.1.1 = {
		 welayta0050
	}
}


c_sharka = {
	867.1.1 = {	change_development_level = 3 }
	1066.1.1 = { change_development_level = 6 }

	758.1.1 = {
		liege = "d_showa"
	 145973
	government = tribal_government
	}
	793.1.1 = {
		 145974
	}
	818.1.1 = {
		 145975
	}
	854.1.1 = {
		 183509
	}
	935.1.1 = {
		government = feudal_government	#Ahistorical, just for ease of bookmarks.
	}
	1040.1.1 = {
		 145774
	}
	1074.1.1 = {
		 145775
	}
	1101.1.1 = {
		 145776
	}
	1141.1.1 = {
		 145777
	}
	1175.1.1 = {
		 145778
	}
	1198.1.1 = {
		 145779
	}
	1239.1.1 = {
		 145780
	}
	1256.1.1 = {
		 145781
	}
	1287.1.1 = {
		 145782
	}
	1331.1.1 = {
		 145783
	}
	1332.1.1 = {
		liege = "d_showa"
	}
}


c_burji = {
	867.1.1 = {	change_development_level = 1 }
	1066.1.1 = { change_development_level = 3 }

	860.1.1 = {
		 welayta0031
		government = tribal_government
	}
	899.1.1 = {
		 welayta0032
	}
	950.1.1 = {
		 welayta0033
	}
	1005.1.1 = {
		 welayta0034
	}
	1058.1.1 = {
		 welayta0035
	}
	1102.1.1 = {
		 welayta0036
	}
	1136.1.1 = {
		 welayta0037
	}
	1178.1.1 = {
		 welayta0038
	}
	1221.1.1 = {
		 welayta0039
	}
	1263.1.1 = {
		 welayta0040
	}
}


c_dara = {
	867.1.1 = {	change_development_level = 1 }
	1066.1.1 = { change_development_level = 3 }

	860.1.1 = {
		 welayta0031
		government = tribal_government
	}
	899.1.1 = {
		 welayta0032
	}
	950.1.1 = {
		 welayta0033
	}
	1005.1.1 = {
		 welayta0034
	}
	1058.1.1 = {
		 welayta0035
	}
	1102.1.1 = {
		 welayta0036
	}
	1136.1.1 = {
		 welayta0037
	}
	1178.1.1 = {
		 welayta0038
	}
	1221.1.1 = {
		 welayta0039
	}
	1263.1.1 = {
		 welayta0040
	}
	# Walashma
	1285.1.1 = {
		government = clan_government
		 32892
	}
	1304.1.1 = {
		 32943
	}
	1321.1.1 = {
		 32944
	}
	1328.1.1 = {
		 32945
	}
	
	# Getachew
	
	1332.1.1 = {
		 145783
	}
}


c_hadya = {
	867.1.1 = {	change_development_level = 3 }
	1066.1.1 = { change_development_level = 6 }

	758.1.1 = {
		liege = "d_showa"
	 145973
	government = tribal_government
	}
	793.1.1 = {
		 145974
	}
	818.1.1 = {
		 145975
	}
	854.1.1 = {
		 183509
	}
	935.1.1 = {
		government = feudal_government	#Ahistorical, just for ease of bookmarks.
	}
	1040.1.1 = {
		 145774
	}
	1074.1.1 = {
		 145775
	}
	1101.1.1 = {
		 145776
	}
	1141.1.1 = {
		 145777
	}
	1175.1.1 = {
		 145778
	}
	1198.1.1 = {
		 145779
	}
	1239.1.1 = {
		 145780
	}
	1256.1.1 = {
		 145781
	}
	1287.1.1 = {
		 145782
	}
	1331.1.1 = {
		 145783
	}
	1332.1.1 = {
		liege = "d_showa"
	}
}

