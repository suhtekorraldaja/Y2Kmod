
k_gujarat = {
	867.1.1 = { change_development_level = 7 }
	1066.1.1 = { change_development_level = 10 }
	
	502.1.1 = {
		 191645 #Dronasimha
	}
	525.1.1 = {
		 191646 #Dhruvasena
	}
	545.1.1 = {
		 191647 #Dharapatta
	}
	556.1.1 = {
		 191648 #Guhasena
	}
	571.1.1 = {
		 191649 #Dharasena
	}
	595.1.1 = {
		 191650 #Siladitya
	}
	616.1.1 = {
		 191651 #Kharagraha
	}
	623.1.1 = {
		 191652 #Dharasena
	}
	629.1.1 = {
		 191653 #Dhruvasena
	}
	640.1.1 = {
		 191654 #Dharasena
	}
	651.1.1 = {
		 191656 #Dhruvasena
	}
	653.1.1 = {
		 191657 #Kharagraha
	}
	662.1.1 = {
		 191659 #Siladitya II
	}
	691.1.1 = {
		 191660 #Siladitya III
	}
	722.1.1 = {
		 191661 #Siladitya IV
	}
	745.1.1 = {
		 0
	}
	1064.1.1 = {
		 188089 # Karnadeva

	}
	1094.1.1 = {
		 188091 # Jayasimha
	}
	1143.1.1 = {
		 188088 # Kumarapala
	}
	1171.1.1 = {
		 188093 # Ajayapala
	}
	1177.1.1 = {
		 188094 # Mulraja II
	}
	1178.1.1 = {
		 188095 # Bhima II
	}
	1208.1.1 = {
		 188097 # Jayasimha
	}
	1224.1.1 = {
		 188095 # Bhima II (again)
	}
	1241.1.1 = {
		 188096 # Tribhuvanapala
	}
	1243.1.1 = {
		 188158 # Viramdeva Vaghela
	}
	1262.1.1 = {
		 188160 # Arjunadeva
	}
	1274.1.1 = {
		 188161 # Sarangadeva
	}
	1296.1.1 = {
		 188163 # Karnadeva
	}
	1304.1.1 = {
		liege = e_rajastan
		 170246 #Ala-ud-din Muhammad
	}
	1316.1.3 = {
		 170247 #Umar
	}
	1316.4.1 = {
		 170248 #Mubarak
	}
	1318.1.1 = {
		 170250 #Mahmud
	}
	1318.6.1 = {
		 170248 #Mubarak
	}
	1320.6.1 = {
		 170251 #Husraw Kushru
	}
	1320.10.1 = {
		 170257 #Ghiyath al-Din Tughluq
	}
	1325.2.1 = {
		 170258 #Muhammad
	}
}


d_gurjara_mandala = {
	867.1.1 = { change_development_level = 12 }
	1066.1.1 = { change_development_level = 18 }
	
	502.1.1 = {
		 191645 #Dronasimha
		liege = k_gujarat
	}
	525.1.1 = {
		 191646 #Dhruvasena
	}
	545.1.1 = {
		 191647 #Dharapatta
	}
	556.1.1 = {
		 191648 #Guhasena
	}
	571.1.1 = {
		 191649 #Dharasena
	}
	595.1.1 = {
		 191650 #Siladitya
	}
	616.1.1 = {
		 191651 #Kharagraha
	}
	623.1.1 = {
		 191652 #Dharasena
	}
	629.1.1 = {
		 191653 #Dhruvasena
	}
	640.1.1 = {
		 191654 #Dharasena
	}
	651.1.1 = {
		 191656 #Dhruvasena
	}
	653.1.1 = {
		 191657 #Kharagraha
	}
	662.1.1 = {
		 191659 #Siladitya II
	}
	691.1.1 = {
		 191660 #Siladitya III
	}
	722.1.1 = {
		 191661 #Siladitya IV
	}
	745.1.1 = {
		 188063 # Vanaraja Capa
		liege = k_malwa
	}
	756.1.1 = {
		liege = 0	#Capas assert independence during weak Pratihara rulers
	}
	780.1.1 = {
		 188064 # Yogaraja
		liege = k_malwa
	}
	815.1.1 = {
		liege = k_kosala
	}
	820.1.1 = {
		 188066 # Ratnaditya
	}
	825.1.1 = {
		 188067 # Vairisimha
	}
	835.1.1 = {
		 188068 # Ksemaraja
	}
	860.1.1 = {
		 188070 # Akadadeva
	}
	890.1.1 = {
		 188072 # Bhuyadadeva
	}
	920.1.1 = {
		 188073 # Bhubhatadeva
	}
	942.1.1 = {
		liege = 0
		 188074 # Mulraja I Solanki
	}
	996.1.1 = {
		 188075 # Chamundaraja
	}
	1008.1.1 = {
		 188076 # Vallabharaja
	}
	1010.1.1 = {
		 188077 # Durlabharaja
	}
	1022.1.1 = {
		 188081 # Bhima I
	}
	1064.1.1 = {
		 188089 # Karnadeva
	}
	1094.1.1 = {
		 188091 # Jayasimha
	}
	1143.1.1 = {
		 188088 # Kumarapala
	}
	1171.1.1 = {
		 188093 # Ajayapala
	}
	1177.1.1 = {
		 188094 # Mulraja II
	}
	1178.1.1 = {
		 188095 # Bhima II
	}
	1208.1.1 = {
		 188097 # Jayasimha
	}
	1224.1.1 = {
		 188095 # Bhima II (again)
	}
	1241.1.1 = {
		 188096 # Tribhuvanapala
	}
	1243.1.1 = {
		 188158 # Viramdeva Vaghela
	}
	1262.1.1 = {
		 188160 # Arjunadeva
	}
	1274.1.1 = {
		 188161 # Sarangadeva
	}
	1296.1.1 = {
		 188163 # Karnadeva
	}
	1304.1.1 = {
		liege = k_delhi
	}
}


c_sarasvata_mandala = {
	745.1.1 = {
		liege = k_gujarat
		 188063 # Vanaraja Capa
	}
	780.1.1 = {
		 188064 # Yogaraja
	}
	820.1.1 = {
		 188066 # Ratnaditya
		liege = d_gurjara_mandala
	}
	825.1.1 = {
		 188067 # Vairisimha
	}
	835.1.1 = {
		 188068 # Ksemaraja
	}
	860.1.1 = {
		 188070 # Akadadeva
	}
	890.1.1 = {
		 188072 # Bhuyadadeva
	}
	920.1.1 = {
		 188073 # Bhubhatadeva
	}
	942.1.1 = {
		 188074 # Mulraja I Solanki
	}
	996.1.1 = {
		 188075 # Chamundaraja
	}
	1008.1.1 = {
		 188076 # Vallabharaja
	}
	1010.1.1 = {
		 188077 # Durlabharaja
	}
	1022.1.1 = {
		 188081 # Bhima I
	}
	1064.1.1 = {
		 188089 # Karnadeva
	}
	1094.1.1 = {
		 188091 # Jayasimha
	}
	1143.1.1 = {
		 188088 # Kumarapala
	}
	1171.1.1 = {
		 188093 # Ajayapala
	}
	1177.1.1 = {
		 188094 # Mulraja II
	}
	1178.1.1 = {
		 188095 # Bhima II
	}
	1208.1.1 = {
		 188097 # Jayasimha
	}
	1224.1.1 = {
		 188095 # Bhima II (again)
	}
	1241.1.1 = {
		 188096 # Tribhuvanapala
	}
	1243.1.1 = {
		 188158 # Viramdeva Vaghela
	}
	1262.1.1 = {
		 188160 # Arjunadeva
	}
	1274.1.1 = {
		 188161 # Sarangadeva
	}
	1296.1.1 = {
		 188163 # Karnadeva
	}
	1304.1.1 = {
		liege = k_delhi
	}
}


c_khetaka = {
	475.1.1 = {
		 191643 #Bhatarka Maitraka
	}
	493.1.1 = {
		 191644 #Dharasena
	}
	502.1.1 = {
		 191645 #Dronasimha (son of Bhatarka)
	}
	525.1.1 = {
		 191646 #Dhruvasena (son of Bhatarka)
	}
	545.1.1 = {
		 191647 #Dharapatta(son of Bhatarka)
	}
	556.1.1 = {
		 191648 #Guhasena
	}
	571.1.1 = {
		 191649 #Dharasena
	}
	595.1.1 = {
		 191650 #Siladitya
	}
	616.1.1 = {
		 191651 #Kharagraha
	}
	623.1.1 = {
		 191652 #Dharasena
	}
	629.1.1 = {
		 191653 #Dhruvasena
	}
	640.1.1 = {
		 191654 #Dharasena
	}
	651.1.1 = {
		 191656 #Dhruvasena
	}
	653.1.1 = {
		 191657 #Kharagraha
	}
	662.1.1 = {
		 191659 #Siladitya II
	}
	691.1.1 = {
		 191660 #Siladitya III
	}
	722.1.1 = {
		 191661 #Siladitya IV
	}
	740.1.1 = {
		liege = d_gurjara_mandala
	}
	760.1.1 = {
		 191662 #Siladitya V
	}
	766.1.1 = {
		 191663 #Siladitya VI
	}
	780.1.1 = {
		 188064 # Yogaraja
	}
	820.1.1 = {
		 188066 # Ratnaditya
	}
	825.1.1 = {
		 188067 # Vairisimha
	}
	835.1.1 = {
		 188068 # Ksemaraja
	}
	860.1.1 = {
		 188070 # Akadadeva
	}
	890.1.1 = {
		 188072 # Bhuyadadeva
	}
	920.1.1 = {
		 188073 # Bhubhatadeva
	}
	942.1.1 = {
		 188074 # Mulraja I Solanki
	}
	996.1.1 = {
		 188075 # Chamundaraja
	}
	1008.1.1 = {
		 188076 # Vallabharaja
	}
	1010.1.1 = {
		 188077 # Durlabharaja
	}
	1022.1.1 = {
		 188081 # Bhima I
	}
	1055.1.1 = {
		 188167 # Asha Bhil
	}
	1068.1.1 = {
		 188089 # Karnadeva
	}
	1094.1.1 = {
		 188091 # Jayasimha
	}
	1143.1.1 = {
		 188088 # Kumarapala
	}
	1171.1.1 = {
		 188093 # Ajayapala
	}
	1177.1.1 = {
		 188094 # Mulraja II
	}
	1178.1.1 = {
		 188095 # Bhima II
	}
	1208.1.1 = {
		 188097 # Jayasimha
	}
	1224.1.1 = {
		 188095 # Bhima II (again)
	}
	1241.1.1 = {
		 188096 # Tribhuvanapala
	}
	1243.1.1 = {
		 188158 # Viramdeva Vaghela
	}
	1262.1.1 = {
		 188160 # Arjunadeva
	}
	1274.1.1 = {
		 188161 # Sarangadeva
	}
	1296.1.1 = {
		 188163 # Karnadeva
	}
	1304.1.1 = {
		liege = k_delhi
	}
}


c_mohadavasaka = {
	760.1.1 = {
		 191920 #Hunas
		liege = d_dadhipadra
	}
	780.1.1 = {
		 191922 #Hunas
	}
	800.1.1 = {
		 188176 # Upendra Paramara
	}
	818.1.1 = {
		 188177 # Vairisimha I
	}
	840.1.1 = {
		 191914 #Dhanika Paramara
	}
	885.1.1 = {
		 191915 #Chachcha
	}
	915.1.1 = {
		 191916 #Dambarasimha
	}
	945.1.1 = {
		 191918 #Kankadeva
	}
	975.1.1 = {
		 191919 #Chandapa
	}
	995.1.1 = {
		 188239 #Satya
	}
	1020.1.1 = {
		 191921 #Limbaraja
	}
	1045.1.1 = {
		 188240 #Mandalika
	}
	1070.1.1 = {
		 188241 #Chamunda
	}
	1101.1.1 = {
		 191924 #Vijaya
	}
	1179.1.1 = {
		 188496 #Samant Singh Guhilot
		liege = d_medapata
	}
	1209.1.1 = {
		 188520 #Jayat Singh
	}
	1218.1.1 = {
		 188521 #Sinhad Deo
	}
	1248.1.1 = {
		 188522 #Jay Singh Deo
	}
	1251.1.1 = {
		 188523 #Deopali Deo
	}
	1257.1.1 = {
		 191887 #Sonam Rathore
		liege = 0
	}
	1275.1.1 = {
		 191905 #Emal
	}
	1300.1.1 = {
		 191906 #Dhavalma
		liege = e_rajastan
	}
	1305.1.1 = {
		liege = k_malwa
	}
	1320.1.1 = {
		 191907 #Lunkaran
	}
	1340.1.1 = {
		 191908 #Kanhat
	}
	1360.1.1 = {
		 191909 #Ranmal
	}
	1400.1.1 = {
		 191910 #Punj
	}

}


b_mohadavasaka = {
	#From 1179 this is used for Dungarpur
	1257.1.1 = {
		 188523 #Deopali Deo
	}
	1278.1.1 = {
		 188524 #Virsingh Deo
	}
	1303.1.1 = {
		 188525 #Bhoo Chand
	}

}


c_dhamalpur = {
	745.1.1 = {
		liege = d_saurashtra
		 188063 # Vanaraja Capa
	}
	780.1.1 = {
		 188064 # Yogaraja
	}
	820.1.1 = {
		 188066 # Ratnaditya
	}
	825.1.1 = {
		 188067 # Vairisimha
	}
	835.1.1 = {
		 188068 # Ksemaraja
	}
	860.1.1 = {
		 188070 # Akadadeva
	}
	890.1.1 = {
		 188072 # Bhuyadadeva
	}
	920.1.1 = {
		 188073 # Bhubhatadeva
	}
	942.1.1 = {
		 188074 # Mulraja I Solanki
	}
	996.1.1 = {
		 188075 # Chamundaraja
	}
	1008.1.1 = {
		 188076 # Vallabharaja
	}
	1010.1.1 = {
		 188077 # Durlabharaja
	}
	1022.1.1 = {
		 188081 # Bhima I
	}
	1064.1.1 = {
		 188089 # Karnadeva
	}
	1094.1.1 = {
		 188091 # Jayasimha
	}
	1143.1.1 = {
		 188088 # Kumarapala
	}
	1170.1.1 = {
		 188154 # Arnoraja Vaghela
	}
	1200.1.1 = {
		 188155 # Lavaṇaprasada
	}
	1232.1.1 = {
		 188157 # Viradhavala
	}
	1238.1.1 = {
		 188158 # Viramdeva
	}
	1262.1.1 = {
		 188160 # Arjunadeva
	}
	1274.1.1 = {
		 188161 # Sarangadeva
	}
	1296.1.1 = {
		 188163 # Karnadeva
	}
	1304.1.1 = {
		liege = k_gujarat
	}
}

d_anartta = {
	867.1.1 = { change_development_level = 5 }
	1066.1.1 = { change_development_level = 8 }
}

c_dvaraka = {
	725.1.1 = {
		liege = d_saurashtra
		 188265 # Pushyadeva Saindhava
	}
	750.1.1 = {
		 188266 # Krishnaraja I
	}
	775.1.1 = {
		 188267 # Agguka I
	}
	800.1.1 = {
		 188268 # Ranaka
	}
	820.1.1 = {
		 188269 # Krishnaraja II
	}
	830.1.1 = {
		 188270 # Agguka II
	}
	835.1.1 = {
		 188271 # Jaika I
	}
	845.1.1 = {
		 188272 # Chamundaraja
	}
	865.1.1 = {
		 188273 # Agguka III
	}
	875.1.1 = {
		 188274 # Ranaka II
	}
	883.1.1 = {
		 188275 # Agguka IV
	}
	904.1.1 = {
		 188276 # Jaika II
	}
	920.1.1 = {
		 188244 # Mulraj Chudasama/Abhira
	}
	940.1.1 = {
		 188245 # Graharipu
	}
	965.1.1 = {
		 188246 # Ra Kawat
	}
	975.1.1 = {
		 188247 # Ra Diyas/Dayaji
	}
	1010.1.1 = {
		liege = k_gujarat
		 188077 # Durlabharaja
	}
	1022.1.1 = {
		 188081 # Bhima I
	}
	1025.1.1 = {
		liege = 0
		 188248 # Ra Navghan
	}
	1044.1.1 = {
		 188249  # Ra Khengar
	}
	1067.1.1 = {
		 188250 # Ra Navghan II
	}
	1088.1.1 = {
		 188251 # Ra Khengar II
	}
	1105.1.1 = {
		liege = k_gujarat
		 188091 # Jayasimha
	}
	1120.1.1 = {
		liege = 0
		 188253 # Ra Navghan III
	}
	1140.1.1 = {
		 188254 # Ra Kawat II
	}
	1152.1.1 = {
		 188255 # Ra Grahar II or Jayasinha
	}
	1180.1.1 = {
		 188256 # Ra Raisinha
	}
	1184.1.1 = {
		 188257 # Ra Mahipal II (aka Gajraj)
	}
	1201.1.1 = {
		 188258 # Ra Jaymal
	}
	1230.1.1 = {
		 188259 # Ra Mahipal III
	}
	1253.1.1 = {
		 188260 # Ra Khengar III
	}
	1260.1.1 = {
		 188261 # Ra Mandlik I
	}
	1304.1.1 = {
		liege = e_rajastan
	}
	1306.1.1 = {
		 188262 # Ra Navghan IV
	}
	1308.1.1 = {
		 188263 # Ra Mahipal IV
	}
	1325.1.1 = {
		 188264 # Ra Khenghar IV
	}
}


c_kutch = {
	#Kathis of Kanthakota
	768.1.1 = {
		 191682
	}
	780.1.1 = {
		 191683
	}
	815.1.1 = {
		liege = d_stravani
		 188303 # Mod Samma
	}
	840.1.1 = {
		 188304 # Saad
	}
	860.1.1 = {
		 188305 # Phool
	}
	# two more Samma rulers: Phoolani Pooeronjee
	# Chawda take over after Pooeronjee
	
	930.1.1 = {
		liege = k_gujarat
		 188073 # Bhubhatadeva
	}
	942.1.1 = {
		 188306 # ? Chawda
	}
	970.1.1 = {
		 188307 # ? Chawda
	}
	1000.1.1 = {
		 188308 # ? Chawda
	}
	1030.1.1 = {
		 188309 # ? Chawda
	}
	1060.1.1 = {
		 188310 # ? Chawda
	}
	1085.1.1 = {
		 188311 # ? Chawda
	}
	1105.1.1 = {
		 188312 # ? Chawda
	}
	1125.1.1 = {
		 188313 # Waghum Chawda
	}
	1147.1.1 = {
		liege = 0
		 188278 # Lakho Jadani
	}
	1175.1.1 = {
		 188279 # Ratto Rayadhan
	}
	1215.1.1 = {
		 188280 # Othoji
	}
	1255.1.1 = {
		 188281 # Rao Gaoji
	}
	1285.1.1 = {
		 188282 # Rao Vehanji
	}
	1321.1.1 = {
		 188283 #Rao Mulvaji
	}
	
	1325.1.1 = {
		liege = e_rajastan
	}
}


d_saurashtra = {
	502.1.1 = {
		 191645 #Dronasimha
		liege = k_gujarat
	}
	525.1.1 = {
		 191646 #Dhruvasena
	}
	545.1.1 = {
		 191647 #Dharapatta
	}
	556.1.1 = {
		 191648 #Guhasena
	}
	571.1.1 = {
		 191649 #Dharasena
	}
	595.1.1 = {
		 191650 #Siladitya
	}
	616.1.1 = {
		 191651 #Kharagraha
	}
	623.1.1 = {
		 191652 #Dharasena
	}
	629.1.1 = {
		 191653 #Dhruvasena
	}
	640.1.1 = {
		 191654 #Dharasena
	}
	651.1.1 = {
		 191656 #Dhruvasena
	}
	653.1.1 = {
		 191657 #Kharagraha
	}
	662.1.1 = {
		 191659 #Siladitya II
	}
	691.1.1 = {
		 191660 #Siladitya III
	}
	722.1.1 = {
		 191661 #Siladitya IV
	}
	745.1.1 = {
		 188063 # Vanaraja Capa
		liege = k_malwa
	}
	756.1.1 = {
		liege = 0	#Capas assert independence during weak Pratihara rulers
	}
	780.1.1 = {
		 188064 # Yogaraja
		liege = k_malwa
	}
	815.1.1 = {
		liege = k_kosala
	}
	820.1.1 = {
		 188066 # Ratnaditya
	}
	825.1.1 = {
		 188067 # Vairisimha
	}
	835.1.1 = {
		 188068 # Ksemaraja
	}
	860.1.1 = {
		 188070 # Akadadeva
	}
	890.1.1 = {
		 188072 # Bhuyadadeva
	}
	920.1.1 = {
		 188073 # Bhubhatadeva
	}
	942.1.1 = {
		 188074 # Mulraja I Solanki
		liege = 0
	}
	996.1.1 = {
		 188075 # Chamundaraja
	}
	1008.1.1 = {
		 188076 # Vallabharaja
	}
	1010.1.1 = {
		 188077 # Durlabharaja
	}
	1022.1.1 = {
		 188081 # Bhima I
	}
	1064.1.1 = {
		 188089 # Karnadeva
	}
	1094.1.1 = {
		 188091 # Jayasimha
	}
	1143.1.1 = {
		 188088 # Kumarapala
	}
	1171.1.1 = {
		 188093 # Ajayapala
	}
	1177.1.1 = {
		 188094 # Mulraja II
	}
	1178.1.1 = {
		 188095 # Bhima II
	}
	1208.1.1 = {
		 188097 # Jayasimha
	}
	1224.1.1 = {
		 188095 # Bhima II (again)
	}
	1241.1.1 = {
		 188096 # Tribhuvanapala
	}
	1243.1.1 = {
		 188158 # Viramdeva Vaghela
	}
	1262.1.1 = {
		 188160 # Arjunadeva
	}
	1274.1.1 = {
		 188161 # Sarangadeva
	}
	1296.1.1 = {
		 188163 # Karnadeva
	}
	1304.1.1 = {
		liege = k_delhi
	}
}


c_bhumilka = {
	725.1.1 = {
		liege = d_saurashtra
		 188265 # Pushyadeva Saindhava
	}
	750.1.1 = {
		 188266 # Krishnaraja I
	}
	775.1.1 = {
		 188267 # Agguka I
	}
	800.1.1 = {
		 188268 # Ranaka
	}
	820.1.1 = {
		 188269 # Krishnaraja II
	}
	830.1.1 = {
		 188270 # Agguka II
	}
	835.1.1 = {
		 188271 # Jaika I
	}
	845.1.1 = {
		 188272 # Chamundaraja
	}
	860.1.1 = {
		 188273 # Agguka III
	}
	874.1.1 = {
		 188274 # Ranaka II
	}
	886.1.1 = {
		 188275 # Agguka IV
	}
	900.1.1 = {
		 188276 # Jaika II
	}
	916.1.1 = {
		 188244 # Mulraj
	}
	940.1.1 = {
		 188245 # Graharipu
	}
	965.1.1 = {
		 188246 # Ra Kawat
	}
	975.1.1 = {
		 188247 # Ra Diyas/Dayaji
	}
	1010.1.1 = {
		liege = k_gujarat
		 188077 # Durlabharaja
	}
	1022.1.1 = {
		 188081 # Bhima I
	}
	1025.1.1 = {
		liege = 0
		 188248 # Ra Navghan
	}
	1044.1.1 = {
		 188249  # Ra Khengar
	}
	1067.1.1 = {
		 188250 # Ra Navghan II
	}
	1088.1.1 = {
		 188251 # Ra Khengar II
	}
	1105.1.1 = {
		liege = k_gujarat
		 188091 # Jayasimha
	}
	1120.1.1 = {
		liege = 0
		 188253 # Ra Navghan III
	}
	1140.1.1 = {
		 188254 # Ra Kawat II
	}
	1152.1.1 = {
		 188255 # Ra Grahar II or Jayasinha
	}
	1180.1.1 = {
		 188256 # Ra Raisinha
	}
	1184.1.1 = {
		 188257 # Ra Mahipal II (aka Gajraj)
	}
	1201.1.1 = {
		 188258 # Ra Jaymal
	}
	1230.1.1 = {
		 188259 # Ra Mahipal III
	}
	1253.1.1 = {
		 188260 # Ra Khengar III
	}
	1260.1.1 = {
		 188261 # Ra Mandlik I
	}
	1304.1.1 = {
		liege = k_gujarat
	}
	1306.1.1 = {
		 188262 # Ra Navghan IV
	}
	1308.1.1 = {
		 188263 # Ra Mahipal IV
	}
	1325.1.1 = {
		 188264 # Ra Khenghar IV
	}

}


c_somnath = {
	740.1.1 = {
		liege = d_saurashtra
		 191672 # Calukya
	}
	760.1.1 = {
		 191673 # Kalla
	}
	780.1.1 = {
		 191674 # Mahalla
	}
	810.1.1 = {
		 191675 # Calukya
	}
	825.1.1 = {
		 191676 # Vahukadhavala
	}
	850.1.1 = {
		 191677 # Avanivarman I
	}
	880.1.1 = {
		 191678 # Balavarman
	}
	895.1.1 = {
		 191679 # Avanivarman
	}
	915.1.1 = {
		 188244 #Abhiras
		liege = 0
	}
	942.1.1 = {
		 188074 # Mulraja I Solanki
		liege = d_saurashtra
	}
	996.1.1 = {
		 188075 # Chamundaraja
	}
	1008.1.1 = {
		 188076 # Vallabharaja
	}
	1010.1.1 = {
		 188077 # Durlabharaja
	}
	1022.1.1 = {
		 188081 # Bhima I
	}
	1064.1.1 = {
		 188089 # Karnadeva
	}
	1094.1.1 = {
		 188091 # Jayasimha
	}
	1143.1.1 = {
		 188088 # Kumarapala
	}
	1171.1.1 = {
		 188093 # Ajayapala
	}
	1177.1.1 = {
		 188094 # Mulraja II
	}
	1178.1.1 = {
		 188095 # Bhima II
	}
	1208.1.1 = {
		 188097 # Jayasimha
	}
	1224.1.1 = {
		 188095 # Bhima II (again)
	}
	1241.1.1 = {
		 188096 # Tribhuvanapala
	}
	1243.1.1 = {
		 188158 # Viramdeva Vaghela
	}
	1262.1.1 = {
		 188160 # Arjunadeva
	}
	1274.1.1 = {
		 188161 # Sarangadeva
	}
	1296.1.1 = {
		 188163 # Karnadeva
	}
	1304.1.1 = {
		liege = k_gujarat
		 170246 #Ala-ud-din Muhammad
	}
	1316.1.3 = {
		 170247 #Umar
	}
	1316.4.1 = {
		 170248 #Mubarak
	}
	1318.1.1 = {
		 170250 #Mahmud
	}
	1318.6.1 = {
		 170248 #Mubarak
	}
	1320.6.1 = {
		 170251 #Husraw Kushru
	}
	1320.10.1 = {
		 170257 #Ghiyath al-Din Tughluq
	}
	1325.2.1 = {
		 170258 #Muhammad
	}
	1330.1.1 = {
		 188264 # Ra Khenghar IV
	}
}


c_vardhamana = {
	502.1.1 = {
		 191645 #Dronasimha
		liege = d_saurashtra
	}
	525.1.1 = {
		 191646 #Dhruvasena
	}
	545.1.1 = {
		 191647 #Dharapatta
	}
	556.1.1 = {
		 191648 #Guhasena
	}
	571.1.1 = {
		 191649 #Dharasena
	}
	595.1.1 = {
		 191650 #Siladitya
	}
	616.1.1 = {
		 191651 #Kharagraha
	}
	623.1.1 = {
		 191652 #Dharasena
	}
	629.1.1 = {
		 191653 #Dhruvasena
	}
	640.1.1 = {
		 191654 #Dharasena
	}
	651.1.1 = {
		 191656 #Dhruvasena
	}
	653.1.1 = {
		 191657 #Kharagraha
	}
	662.1.1 = {
		 191659 #Siladitya II
	}
	691.1.1 = {
		 191660 #Siladitya III
	}
	722.1.1 = {
		 191661 #Siladitya IV
	}
	745.1.1 = {
		 188063 # Vanaraja Capa
	}
	760.1.1 = {
		 191664 #Mahavaraha Varaha
	}
	780.1.1 = {
		 191665 #Jayavaraha Varaha
	}
	800.1.1 = {
		 191666 #Vikramarka Capa (Vardhamana branch)
	}
	840.1.1 = {
		 191667 #Addaka Capa (Vardhamana branch)
	}
	860.1.1 = {
		 191668 #Pulakesi Capa (Vardhamana branch)
	}
	880.1.1 = {
		 191669 #Dhruvabhata Capa (Vardhamana branch)
	}
	900.1.1 = {
		 191670 #Dharanivaraha Capa (Vardhamana branch)
		liege = k_malwa
	}
	942.1.1 = {
		 188074 # Mulraja I Solanki
		liege = d_saurashtra
	}
	996.1.1 = {
		 188075 # Chamundaraja
	}
	1008.1.1 = {
		 188076 # Vallabharaja
	}
	1010.1.1 = {
		 188077 # Durlabharaja
	}
	1022.1.1 = {
		 188081 # Bhima I
	}
	1064.1.1 = {
		 188089 # Karnadeva
	}
	1094.1.1 = {
		 188091 # Jayasimha
	}
	1143.1.1 = {
		 188088 # Kumarapala
	}
	1170.1.1 = {
		 188154 # Arnoraja Vaghela
	}
	1200.1.1 = {
		 188155 # Lavaṇaprasada
	}
	1232.1.1 = {
		 188157 # Viradhavala
	}
	1238.1.1 = {
		 188158 # Viramdeva
	}
	1262.1.1 = {
		 188160 # Arjunadeva
	}
	1274.1.1 = {
		 188161 # Sarangadeva
	}
	1296.1.1 = {
		 188163 # Karnadeva
	}
	1304.1.1 = {
		liege = k_delhi
	}
}


c_valabhi = {
	475.1.1 = {
		 191643 #Bhatarka Maitraka
	}
	493.1.1 = {
		 191644 #Dharasena
	}
	502.1.1 = {
		 191645 #Dronasimha
	}
	525.1.1 = {
		 191646 #Dhruvasena
	}
	545.1.1 = {
		 191647 #Dharapatta
	}
	556.1.1 = {
		 191648 #Guhasena
	}
	571.1.1 = {
		 191649 #Dharasena
	}
	595.1.1 = {
		 191650 #Siladitya
	}
	616.1.1 = {
		 191651 #Kharagraha
	}
	623.1.1 = {
		 191652 #Dharasena
	}
	629.1.1 = {
		 191653 #Dhruvasena
	}
	640.1.1 = {
		 191654 #Dharasena
	}
	651.1.1 = {
		 191656 #Dhruvasena
	}
	653.1.1 = {
		 191657 #Kharagraha
	}
	662.1.1 = {
		 191659 #Siladitya II
	}
	691.1.1 = {
		 191660 #Siladitya III
	}
	722.1.1 = {
		 191661 #Siladitya IV
	}
	745.1.1 = {
		liege = d_saurashtra
		 188063 # Vanaraja Capa
	}
	780.1.1 = {
		 188064 # Yogaraja
	}
	820.1.1 = {
		 188066 # Ratnaditya
	}
	825.1.1 = {
		 188067 # Vairisimha
	}
	835.1.1 = {
		 188068 # Ksemaraja
	}
	860.1.1 = {
		 188070 # Akadadeva
	}
	890.1.1 = {
		 188072 # Bhuyadadeva
	}
	920.1.1 = {
		 188073 # Bhubhatadeva
	}
	942.1.1 = {
		 188074 # Mulraja I Solanki
	}
	996.1.1 = {
		 188075 # Chamundaraja
	}
	1008.1.1 = {
		 188076 # Vallabharaja
	}
	1010.1.1 = {
		 188077 # Durlabharaja
	}
	1022.1.1 = {
		 188081 # Bhima I
	}
	1064.1.1 = {
		 188084 # Xemraja, brother of Karnadeva
	}
	1100.1.1 = {
		 188086 # Haripala
	}
	1105.1.1 = {
		 188087 # Tribhuvanapala
	}
	1127.1.1 = {
		 188091 # Jayasimha
	}
	1143.1.1 = {
		 188088 # Kumarapala
	}
	1171.1.1 = {
		 188093 # Ajayapala
	}
	1177.1.1 = {
		 188094 # Mulraja II
	}
	1178.1.1 = {
		 188095 # Bhima II
	}
	1208.1.1 = {
		 188097 # Jayasimha
	}
	1224.1.1 = {
		 188095 # Bhima II (again)
	}
	1241.1.1 = {
		 188096 # Tribhuvanapala
	}
	1243.1.1 = {
		 188158 # Viramdeva Vaghela
	}
	1262.1.1 = {
		 188160 # Arjunadeva
	}
	1274.1.1 = {
		 188161 # Sarangadeva
	}
	1296.1.1 = {
		 188163 # Karnadeva
	}
	1304.1.1 = {
		liege = k_delhi
	}
}


d_lata = {
	867.1.1 = { change_development_level = 18 }
	1066.1.1 = { change_development_level = 25 }
	
	570.1.1 = {
		 191624 #Dadda I
		liege = k_gujarat
	}
	595.1.1 = {
		 191625 #Jayabhata I Pratihara
	}
	620.1.1 = {
		 191626 #Dadda II Pratihara
	}
	645.1.1 = {
		 191627 #Jayabhata II Pratihara
	}
	665.1.1 = {
		 191628 #Dadda III Pratihara
	}
	695.1.1 = {
		 191629 #Jayabhata III Pratihara
		liege = k_rajputana
	}
	715.1.1 = {
		 191630 #Ahirola Pratihara
	}
	720.1.1 = {
		 191631 #Jayabhata IV Pratihara
	}
	738.1.1 = {
		 191635 #Annexed by Calukyas of Lata
		liege = k_karnata
	}
	747.1.1 = {
		 74409 #Conquered by Dantidurga
	}
	756.1.1 = {
		 191623 #Karka Rashtrakuta, grant by Dantidurga
	}
	800.1.1 = {
		 74416 #Govinda III, back to main branch
	}
	807.1.1 = {
		 74430 #Indra Rashtrakuta (Start of Lata branch)
	}
	818.1.1 = {
		 74431 #Karka
	}
	826.1.1 = {
		 74432 #Govinda
	}
	835.1.1 = {
		 74433 #Dhruva
	}
	845.1.1 = {
		 74434 #Akalavarsha
	}
	867.1.1 = {
		 74435 #Dhruva
	}
	871.1.1 = {
		 74776 #Dantivarman
	}
	887.1.1 = {
		 74777 #Krishma
	}
	910.1.1 = {
		 0
		liege = 0
	}
	946.1.1 = {
		 191680 #Barappa, later Chalukyas of Lata
		liege = k_karnata
	}
	990.1.1 = {
		 191681 #Gongiraja
	}
	1000.1.1 = {
		 188075 #Chamundaraja Solanki
		liege = k_gujarat
	}
	1005.1.1 = {
		 191681 #Gongiraja
	}
	1010.1.1 = {
		 chalukyas_of_lata_1
	}
	1018.1.1 = {
		liege = k_malwa
	}
	1030.1.1 = {
		 chalukyas_of_lata_2
	}
	1050.1.1 = {
		 chalukyas_of_lata_3
	}
	1070.1.1 = {
		 0
		liege = 0
	}

}


c_navasarika = {
	570.1.1 = {
		 191624 #Dadda I Pratihara
		liege = d_lata
	}
	595.1.1 = {
		 191625 #Jayabhata I Pratihara
	}
	620.1.1 = {
		 191626 #Dadda II Pratihara
	}
	645.1.1 = {
		 191627 #Jayabhata II Pratihara
	}
	665.1.1 = {
		 191628 #Dadda III Pratihara
	}
	695.1.1 = {
		 191629 #Jayabhata III Pratihara
	}
	715.1.1 = {
		 191630 #Ahirola Pratihara
	}
	720.1.1 = {
		 191631 #Jayabhata IV Pratihara
	}
	738.1.1 = {
		 191635 #Annexed by Calukyas of Lata
	}
	757.1.1 = {
		 191642 #Bhartrvaddha II Chahamana
		liege = d_dadhipadra
	}
	807.1.1 = {
		 74430 #Indra Rashtrakuta
		liege = d_lata
	}
	818.1.1 = {
		 74431 #Karka Rashtrakuta
	}
	826.1.1 = {
		 74432 #Govinda
	}
	835.1.1 = {
		 74433 #Dhruva
	}
	845.1.1 = {
		 74434 #Akalavarsha
	}
	867.1.1 = {
		 74435 #Dhruva
	}
	871.1.1 = {
		 74776	#Dantivarman
	}
	887.1.1 = {
		 74777	#Krishna
	}
	946.1.1 = {
		 191680 #Barappa
	}
	990.1.1 = {
		 191681 #Gongiraja
	}
	1010.1.1 = {
		 chalukyas_of_lata_1
	}
	1030.1.1 = {
		 chalukyas_of_lata_2
	}
	1050.1.1 = {
		 chalukyas_of_lata_3
	}
	#Ruler Placeholder until something better#
	1070.1.1 = {
		 188186
		liege = k_malwa
	}
	1087.1.1 = {
		 188187
	}
	1094.1.1 = {
		 188188
	}
	1133.1.1 = {
		 188189
	}
	1142.1.1 = {
		 188190
	}
	1143.1.1 = {
		 188191
	}
	1151.1.1 = {
		 188088
	}
	1160.1.1 = {
		 188197
	}
	1193.1.1 = {
		 188198
	}
	1210.1.1 = {
		 188199
	}
	1218.1.1 = {
		 188196
	}
	1239.1.1 = {
		 188200
	}
	1256.1.1 = {
		 188201
	}
	1269.1.1 = {
		 188202
	}
	1274.1.1 = {
		 188203
	}
	1283.1.1 = {
		 188204
	}
	1298.1.1 = {
		 188205
	}
	1305.1.1 = {
		 170246
	}
	1316.1.3 = {
		 170247
	}
	1316.4.1 = {
		 170248
	}
	1318.1.1 = {
		 170250
	}
	1318.6.1 = {
		 170248
	}
	1320.6.1 = {
		 170251
	}
	1320.10.1 = {
		 170257
	}
	1325.2.1 = {
		 170258
	}
	#End of Ruler Placeholder until something better#
}


c_daman = {
	#Up to 670 under the Sendrakas and then the Maitrakas
	670.1.1 = {
		 191632 #Jayasimha Chalukya (of Lata)
		liege = d_nasikya
	}
	690.1.1 = {
		 191634 #Mangalarasa
	}
	738.1.1 = {
		 191635 #Pulakesin
	}
	747.1.1 = {
		 74409 #Conquered by Dantidurga
		liege = d_lata
	}
	756.1.1 = {
		 191623 #Karka Rashtrakuta, grant by Dantidurga
	}
	800.1.1 = {
		 74416#Govinda III, back to main branch
	}
	807.1.1 = {
		 74430 #Indra Rashtrakuta (Start of Lata branch)
	}
	818.1.1 = {
		 74431 #Karka
	}
	826.1.1 = {
		 74432 #Govinda
	}
	835.1.1 = {
		 74433 #Dhruva
	}
	845.1.1 = {
		 74434 #Akalavarsha
	}
	867.1.1 = {
		 74435 #Dhruva
	}
	871.1.1 = {
		 74634
		liege = d_devagiri
	}
	874.1.1 = {
		 74635
	}
	900.1.1 = {
		 74636
	}
	925.1.1 = {
		 74637
	}
	950.1.1 = {
		 74638
	}
	974.1.1 = {
		 74639
	}
	975.1.1 = {
		 74640
	}
	1005.1.1 = {
		 74641
	}
	1020.1.1 = {
		 74642
	}
	1055.1.1 = {
		 74645
	}
	1085.1.1 = {
		 74646
	}
	1115.1.1 = {
		 74648
	}
	1145.1.1 = {
		 74649
	}
	1150.1.1 = {
		 74652
	}
	1165.1.1 = {
		 74653
	}
	1173.1.1 = {
		 74654
		liege = k_maharastra
	}
	1192.1.1 = {
		 74655
	}
	1200.1.1 = {
		 74656
	}
	1220.1.1 = {
		 74514
	}
	1247.1.1 = {
		 74657
	}
	1261.1.1 = {
		 74658
	}
	1271.1.1 = {
		 74659
	}
	1272.1.1 = {
		 74660
	}
	1311.1.1 = {
		 74661
	}
	1312.1.1 = {
		 74662
	}
}


c_vadodara = {
	570.1.1 = {
		 191624 #Dadda I Pratihara
		liege = d_lata
	}
	595.1.1 = {
		 191625 #Jayabhata I Pratihara
	}
	620.1.1 = {
		 191626 #Dadda II Pratihara
	}
	640.1.1 = {
		 191637 #Mahesvardama Chahamana
	}
	650.1.1 = {
		 191638 #Bhimadama
	}
	680.1.1 = {
		 191639 #Bhartrvaddha
	}
	700.1.1 = {
		 191640 #Haradama
	}
	730.1.1 = {
		 191641 #Dhrubhata
	}
	738.1.1 = {
		liege = d_dadhipadra
	}
	757.1.1 = {
		 191642 #Bhartrvaddha
	}
	800.1.1 = {
		liege = k_karnata
		 188176 # Upendra Paramara
	}
	818.1.1 = {
		 188177 # Vairisimha I
	}
	843.1.1 = {
		 188178 # Siyaka I
	}
	893.1.1 = {
		 188179 # Vakpati I
	}
	918.1.1 = {
		liege = k_malwa
		 188180 # Vairisimha II
	}
	946.1.1 = {
		 191680 #Barappa
		liege = d_lata
	}
	990.1.1 = {
		 191681 #Gongiraja
	}
	1010.1.1 = {
		 chalukyas_of_lata_1
	}
	1030.1.1 = {
		 chalukyas_of_lata_2
	}
	1050.1.1 = {
		 chalukyas_of_lata_3
	}
	#Ruler Placeholder until something better#
	1070.1.1 = {
		 188186
		liege = k_malwa
	}
	1087.1.1 = {
		 188187 # Lakshamanadeva
	}
}

