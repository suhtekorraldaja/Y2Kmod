k_hellas = {
	1066.1.1 = {
		 0
	}
}


d_athens = {
	867.1.1 = { change_development_level = 10 }
	1066.1.1 = { change_development_level = 15 }
	
	330.1.1 = {
		liege = "e_byzantium"
	}
	733.1.1 = {
		 145802
	}
	765.1.1 = {
		 145804
	}
	788.1.1 = {
		 145805
	}
	812.1.1 = {
		 145148
	}
	820.1.1 = {
		 145149
	}
	845.1.1 = {
		 145150
	}
	1060.1.1 = {
		 41303
	}
	1107.2.1 = {
		 34576
	}
	1120.1.1 = {
		 34577
	}
	1150.1.1 = {
		 34578
	}
	1176.1.1 = {
		 215013
	}
	1204.5.16 = { # Collapse of Byzantine Empire
		liege = 0
	}
	1204.8.1 = {
		liege = "e_latin_empire"
	}
	1204.8.1 = {
		 31211 #Othon I
	}
	1216.6.11 = {
		liege = 0
	}
	1225.1.1 = {
		 31216 #Guy I
	}
	1263.1.1 = {
		 31217 #Jean I
	}
	1280.1.1 = {
		 31218 #Guillaume I
	}
	1287.1.1 = {
		 31223 #Guy II
	}
	1308.10.5 = {
		 461733 #Gauthier I
	}
	1311.3.15 = {
		 31225 #Roger
	}
	1312.1.1 = {
		liege = "k_trinacria"
	}
	1312.1.1 = {
		 71753 #Manfredo
	}
	1317.11.19 = {
		 461510 #Gugliemo
	}

}


c_attica = {
	330.1.1 = {
		liege = "d_athens"
	}
	733.1.1 = {
		 145802
	}
	765.1.1 = {
		 145804
	}
	788.1.1 = {
		 145805
	}
	812.1.1 = {
		 145148
	}
	820.1.1 = {
		 145149
	}
	845.1.1 = {
		 145150
	}
	1060.1.1 = {
		 41303
	}
	1107.2.1 = {
		 34576
	}
	1120.1.1 = {
		 34577
	}
	1150.1.1 = {
		 34578
	}
	1176.1.1 = {
		 215013
	}
	1204.8.1 = {
		 31211 #Othon I
	}
	1225.1.1 = {
		 31216 #Guy I
	}
	1263.1.1 = {
		 31217 #Jean I
	}
	1280.1.1 = {
		 31218 #Guillaume I
	}
	1287.1.1 = {
		 31223 #Guy II
	}
	1308.10.5 = {
		 461733 #Gauthier I
	}
	1311.3.15 = {
		 31225 #Roger
	}
	1312.1.1 = {
		 71753 #Manfredo
	}
	1317.11.19 = {
		 461510 #Gugliemo
	}

}


c_boeotia = {
	330.1.1 = {
		liege = "d_athens"
	}
	733.1.1 = {
		 145802
	}
	765.1.1 = {
		 145804
	}
	788.1.1 = {
		 145805
	}
	812.1.1 = {
		 145148
	}
	820.1.1 = {
		 145149
	}
	845.1.1 = {
		 145150
	}
	1060.1.1 = {
		 41303
	}
	1107.2.1 = {
		 34576
	}
	1120.1.1 = {
		 34577
	}
	1150.1.1 = {
		 34578
	}
	1176.1.1 = {
		 215013
	}
	1204.8.1 = {
		 31211 #Othon I
	}
	1225.1.1 = {
		 31216 #Guy I
	}
	1263.1.1 = {
		 31217 #Jean I
	}
	1280.1.1 = {
		 31218 #Guillaume I
	}
	1287.1.1 = {
		 31223 #Guy II
	}
	1308.10.5 = {
		 461733 #Gauthier I
	}
	1311.3.15 = {
		 31225 #Roger
	}
	1312.1.1 = {
		 71753 #Manfredo
	}
	1317.11.19 = {
		 461510 #Gugliemo
	}

}


c_euboea = {
	330.1.1 = {
		liege = "d_aegean_islands"
	}
	768.1.1 = {
		 145821
	}
	865.1.1 = {
		 145188
	}
	1040.1.1 = {
		 20513
	}
	1075.1.1 = {
		 70403
	}
	1097.1.1 = {
		 70402
	}
	1100.1.1 = {
		 70404
	}
	1115.1.1 = {
		 232507
	}
	1130.1.1 = {
		 232542
	}
	1146.1.2 = {
		 215019
	}
	1187.1.1 = {
		 215019
	}
	1194.1.1 = {
		 34414
	}
	#4th Crusade
	1204.8.1 = {
		liege = "d_thessalonika"
	}
	1204.8.1 = {
		 31475
	}
	1209.1.1 = {
		 31488
	}
	1216.1.1 = {
		 212072
	}
	1216.1.1 = {
		liege = "d_athens"
	}
	1268.1.1 = {
		 70751
	}
	1275.1.1 = {
		 31482
	}
	1297.1.1 = {
		 461570
	}
	1311.3.15 = {
		liege = "k_venice"
	}
	1317.1.1 = {
		 70738
	}
	1326.1.1 = {
		 31497
	}
}


d_achaia = {
	330.1.1 = {
		liege = e_byzantium
	}
	736.1.1 = {
		 145807
	}
	775.1.1 = {
		 145808
	}
	865.1.1 = {
		 145182
	}
	1060.1.1 = {
		 1766
	}
	1089.1.1 = {
		 232504
	}
	1107.1.1 = {
		 231057
	}
	1151.1.1 = {
		 215010
	}
	1204.5.16 = { # Collapse of Byzantine Empire
		liege = 0
	}
	1204.8.1 = {
		liege = e_latin_empire
	}
	1204.8.1 = {
		 31334 #Guillaume I
	}
	1210.1.1 = {
		 31336 #Geoffory I
	}
	1216.6.11 = {
		liege = 0
	}
	1218.9.1 = {
		 31337 #Geoffory II
	}
	1246.5.6 = {
		 31338 #Guillaume II
	}
	1278.1.4 = {
		liege = k_sicily
	}
	1278.1.14 = {
		 455406 #Charles I d'Anjou
	}
	1285.1.7 = {
		 461710 #Charles II d'Anjou
	}
	1289.1.1 = {
		 31103 #Floris I d'Avesnes
	}
	1297.1.23 = {
		 31341 #Isabelle I
	}
	1301.1.1 = {
		 462691 #Phillipe I
	}
	1302.8.31 = {
		liege = k_naples
	}
	1307.1.1 = {
		 461712 #Phillipe II
	}
	1313.1.1 = {
		 31344 #Louis I
	}
	1316.8.2 = {
		 31343 #Mathilda
	}
	1318.1.1 = {
		 455910 #Eudes
	}
	1320.1.1 = {
		 455650 #Louis II
	}
	1322.1.1 = {
		 461721 #Jean I
	}
	1336.1.2 = {
		 461718 #Robert I
	}
}


c_achaia = {
	330.1.1 = {
		liege = "d_achaia"
	}
	762.1.1 = {
		 145809
	}
	865.1.1 = {
		 145183
	}
	1060.1.1 = {
		 1766
	}
	1089.1.1 = {
		 232504
	}
	1107.1.1 = {
		 231057
	}
	1151.1.1 = {
		 215010
	}
	1204.8.1 = {
		 31334 #Guillaume I
	}
	1210.1.1 = {
		 31336 #Geoffory I
	}
	1218.9.1 = {
		 31337 #Geoffory II
	}
	1246.5.6 = {
		 31338 #Guillaume II
	}
	1278.1.14 = {
		 455406 #Charles I d'Anjou
	}
	1285.1.7 = {
		 461710 #Charles II d'Anjou
	}
	1289.1.1 = {
		 31103 #Floris I d'Avesnes
	}
	1297.1.23 = {
		 31341 #Isabelle I
	}
	1301.1.1 = {
		 462691 #Phillipe I
	}
	1307.1.1 = {
		 461712 #Phillipe II
	}
	1313.1.1 = {
		 31344 #Louis I
	}
	1316.8.2 = {
		 31343 #Mathilda
	}
	1318.1.1 = {
		 455910 #Eudes
	}
	1320.1.1 = {
		 455650 #Louis II
	}
	1322.1.1 = {
		 461721 #Jean I
	}
	1332.1.1 = {
		 461718 #Robert I
	}
}


c_korinthos = {
	330.1.1 = {
		liege = "d_achaia"
	}
	736.1.1 = {
		 145807
	}
	775.1.1 = {
		 145808
	}
	865.1.1 = {
		 145182
	}
	1060.1.1 = {
		 20505
	}
	1075.1.1 = {
		 70426
	}
	1100.1.1 = {
		 70420
	}
	1111.1.1 = {
		 1742
	}
	1118.8.15 = {
		 223023
	}
	1143.4.8 = {
		 215530
	}
	1180.9.24 = {
		 215531
	}
	1183.9.24 = {
		 215529
	}
	1185.9.12 = {
		 215500
	}
	1187.1.1 = {
		 215012
	}
	1198.1.1 = {
		 232502
	}
	1204.8.1 = {
		 31334 #Guillaume I
	}
	1210.1.1 = {
		 31336 #Geoffory I
	}
	1218.9.1 = {
		 31337 #Geoffory II
	}
	1246.5.6 = {
		 31338 #Guillaume II
	}
	1278.1.14 = {
		 455406 #Charles I d'Anjou
	}
	1285.1.7 = {
		 461710 #Charles II d'Anjou
	}
	1289.1.1 = {
		 31103 #Floris I d'Avesnes
	}
	1297.1.23 = {
		 31341 #Isabelle I
	}
	1301.1.1 = {
		 462691 #Phillipe I
	}
	1307.1.1 = {
		 461712 #Phillipe II
	}
	1313.1.1 = {
		 31344 #Louis I
	}
	1316.8.2 = {
		 31343 #Mathilda
	}
	1318.1.1 = {
		 455910 #Eudes
	}
	1320.1.1 = {
		 455650 #Louis II
	}
	1322.1.1 = {
		 461721 #Jean I
	}
	1332.1.1 = {
		 461718 #Robert I
	}
	

}


c_messenia = {
	330.1.1 = {
		liege = "d_achaia"
	}
	767.1.1 = {
		 145810
	}
	866.1.1 = {
		 145184
	}
	1060.1.1 = {
		 34606
	}
	1095.1.1 = {
		 34607
	}
	1110.1.1 = {
		 34608
	}
	1135.1.1 = {
		 34609
	}
	1195.1.1 = {
		 34611
	}
	1204.8.1 = {
		liege = "k_venice"
	}
	1204.8.1 = {
		 213601 #Enrico Dandolo
	}
	1205.6.21 = {
		 32120 #Pietro Ziani
	}
	1229.3.13 = {
		 32124 #Iacopo Tiepolo
	}
	1249.7.19 = {
		 32125 #Marino Morosini
	}
	1253.1.1 = {
		 32126 #Reniero Zeno
	}
	1268.7.7 = {
		 32127 #Lorenzo Tiepolo
	}
	1275.8.15 = {
		 32128 #Iacopo Contarini
	}
	1280.3.6 = {
		 32129 #Giovanni Dandalo
	}
	1289.11.2 = {
		 32130 #Pietro Gradenigo
	}
	1311.8.13 = {
		 32131 #Marino Zorzi
	}
	1312.7.3 = {
		 32132 #Giovannia Soranza
	}
	1328.12.31 = {
		 463600 #Francesco Dandalo
	}
}


c_laconia = {
	867.1.1 = {	change_development_level = 8 }
	1066.1.1 = { change_development_level = 12 }
	
	330.1.1 = {
		liege = "d_achaia"
	}
	764.1.1 = {
		 145811
	}
	865.1.1 = {
		 145107
	}
	1060.1.1 = {
		 20505
	}
	1075.1.1 = {
		 70427
	}
	1076.1.1 = {
		 34428
	}
	1089.1.1 = {
		 70426
	}
	1100.1.1 = {
		 70420
	}
	1111.1.1 = {
		 1742
	}
	1118.8.15 = {
		 223023
	}
	1143.4.8 = {
		 215530
	}
	1165.1.1 = {
		 215011
	}
	
	#de Villehardouin
	
	1204.8.1 = {
		 31336 #Geoffory I
	}
	1218.9.1 = {
		 31337 #Geoffory II
	}
	1246.5.6 = {
		 31338 #Guillaume II
	}
	1262.1.1 = {
		liege = "e_byzantium"
	}
	1262.1.1 = {
		 34422
	}
	1264.3.1 = {
		 34423
	}
	1294.1.1 = {
		 34425
	}
	1305.1.1 = {
		 34424
	}
	1311.1.1 = {
		 465550
	}
}

