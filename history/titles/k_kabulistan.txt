
k_kabulistan = {
	867.1.1 = {	change_development_level = 5 }
	1066.1.1 = { change_development_level = 8 }

	977.1.1 = {
		 144123 # Sabuktigin
	}
	997.1.1 = {
		 144124 # Ismail
	}
	998.1.1 = {
		 144125 # Mahmud
	}
	1030.1.1 = {
		 144126 # Muhammad
	}
	1031.1.1 = {
		 144127 # Musud
	}
	1041.1.1 = {
		 144128 # Mawdud
	}
	1050.1.1 = {
		 144129 # Masud
	}
	1050.6.1 = {
		 144130 # Ali
	}
	1050.9.1 = {
		 144131 # Abd al-Rashid
	}
	1053.1.1 = {
		 144132 # Farrukhzad
	}
	1059.1.1 = {
		 144133 # Ibrahim
		effect = {
			if = {
				limit = {
					exists = holder
					has_dlc_feature = royal_court
				}
				 {
					set_court_language = language_iranian
					if = {
						limit = { NOT = { knows_court_language_of = this } }
						learn_court_language_of = this
					}
				}
			}
		}
	}
	1099.1.1 = {
		 144134 # Masud II
	}
	1115.1.1 = {
		 144135 # Shirzad
	}
	1115.6.1 = {
		 144136 # Arlsan Shah
	}
	1118.1.1 = {
		 0 # Ghaznavid vassalage under Seljuks
	}
	
	1157.1.1 = {
		 144138 # Khusraw Shah
	}
	1160.1.1 = {
		 144139 # Khusraw Malik
	}
	1173.1.1 = {
		 144141 # Ghiyath Ghurid
	}
	1186.1.1 = {
		liege = e_rajastan
	}
	1203.1.1 = {
		 144142 # Mu'izz
	}
	1206.3.15 = {
		 170302
		liege = 0
	}
	1216.2.15 = {
		 0
	}
}


d_kabul = {
	687.1.1 = {
		liege = 0
		 188717 # Khorasan Tegin Shah
	}
	740.1.1 = {
		 188718 # Phrom Kesar Nandin of Kabul Shahi
	}
	745.1.1 = {
		 188565 # Bo-Fuzun Nandin of Kabul Shahi
	}
	
	866.1.1 = {
		 175060 # Kallar Shahi
		government = feudal_government	#Ahistorical, just for ease of bookmarks.
	}
	
	961.1.1 = {
		 170303 #Took Ghazna from Lawiks
		government = clan_government
	}
	975.1.1 = {
		 170304
	}
	977.1.1 = {
		liege = k_kabulistan
		 144123 # Sabuktigin Ghaznavid
	}
	997.1.1 = {
		 144124 # Ismail
	}
	998.1.1 = {
		 144125 # Mahmud
	}
	1030.1.1 = {
		 144126 # Muhammad
	}
	1031.1.1 = {
		 144127 # Musud
	}
	1041.1.1 = {
		 144128 # Mawdud
	}
	1050.1.1 = {
		 144129 # Masud
	}
	1050.6.1 = {
		 144130 # Ali
	}
	1050.9.1 = {
		 144131 # Abd al-Rashid
	}
	1053.1.1 = {
		 144132 # Farrukhzad
	}
	1059.1.1 = {
		 144133 # Ibrahim
	}
	1099.1.1 = {
		 144134 # Masud II
	}
	1115.1.1 = {
		 144135 # Shirzad
	}
	1115.6.1 = {
		 144136 # Arlsan Shah
	}
	1118.1.1 = {
		liege = k_persia # Seljuk vassalage
		 144137 # Bahram Shah
	}
	1152.1.1 = {
		 144138 # Khusraw Shah
	}
	1157.5.8 = {
		liege = 0 # end of the Seljuk Turks
	}
	1160.1.1 = {
		 144139 # Khusraw Malik
	}
	1173.1.1 = {
		liege = k_kabulistan
		 144142 # Mu'izz Ghurid
	}
	1203.1.1 = {
		liege = e_rajastan
	}
	1206.3.15 = {
		 170302
		liege = k_kabulistan
	}
	1216.2.15 = {
		 0
		liege = 0
	}
}


c_kabul = {
	867.1.1 = {	change_development_level = 8 }
	1066.1.1 = { change_development_level = 12 }
	
	750.1.1 = {
		liege = d_kabul
		 188565 # Surendravirkramaditya Nandin of Kabul Shahi
	}
	
	866.1.1 = {
		 175060 # Kallar Shahi
		government = clan_government	#Ahistorical, just for ease of bookmarks.
	}
	
	961.1.1 = {
		 170303 #Took Ghazna from Lawiks
	}
	975.1.1 = {
		 170304
	}
	977.1.1 = {
		 144123
	}
	997.1.1 = {
		 144124
	}
	998.1.1 = {
		 144125
	}
	1030.1.1 = {
		 144126
	}
	1031.1.1 = {
		 144127
	}
	1041.1.1 = {
		 144128
	}
	1050.1.1 = {
		 144129
	}
	1050.6.1 = {
		 144130
	}
	1050.9.1 = {
		 144131
	}
	1053.1.1 = {
		 144132
	}
	1059.1.1 = {
		 144133
	}
	1099.1.1 = {
		 144134
	}
	1115.1.1 = {
		 144135
	}
	1115.6.1 = {
		 144136
	}
	1118.1.1 = {
		 144137
	}
	1152.1.1 = {
		 144138
	}
	1160.1.1 = {
		 144139
	}
	1173.1.1 = {
		 144142
	}
	1206.3.15 = {
		 170302
	}
	1216.2.15 = {
		 93052
		liege = k_persia
	}
	1220.1.1 = {
		 194302
	}
}


c_bamian = {
	663.1.1 = {
		liege = d_merv
	}
	671.1.1 = {
		liege = 0 # Nazak Tarkhan's rule
	}
	715.1.1 = {
		liege = d_merv
	}
	
	737.1.1 = {
		 188637 # Saman-Khuda Samanid
	}
	785.1.1 = {
		 163153 # Asad Samanid
	}
	825.1.1 = {
		 163154 # Ahmad Samanid
	}
	864.1.1 = {
		liege = k_transoxiana
	}
	864.1.1 = {
		 163099 # Nasr Samanid
	}
	977.1.1 = {
		liege = "d_sistan"
	}
	977.1.1 = {
		 144123
	}
	997.1.1 = {
		 144124
	}
	998.1.1 = {
		 144125
	}
	1030.1.1 = {
		 144126
	}
	1031.1.1 = {
		 144127
	}
	1041.1.1 = {
		 144128
	}
	1050.1.1 = {
		 144129
	}
	1050.6.1 = {
		 144130
	}
	1050.9.1 = {
		 144131
	}
	1053.1.1 = {
		 144132
	}
	1059.1.1 = {
		 144133
	}
	1099.1.1 = {
		 144134
	}
	1100.1.1 = {
		liege = "d_merv"
		 93139
	}
	1140.1.1 = {
		 93140
	}
	1149.1.1 = {
		 93150
	}
	
	1180.1.1 = {
		 93151
	}
	1186.1.1 = {
		liege = d_sistan
		 144141
	}
	1203.1.1 = {
		 144142
	}
	1206.3.15 = {
		liege = d_kabul
		 170302
	}
	1212.1.1 = {
		liege = "k_transoxiana"
		 93053
	}
	1220.2.1 = {
		liege = e_mongol_empire
		 125501
	}
	1227.12.2 = {
		 93062
	}
	1242.1.1 = {
		 93064
	}
	1246.1.1 = {
		 93065
	}
	1252.1.1 = {
		 93064
	}
	1253.1.1 = {
		 93066
	}
	1260.1.1 = {
		 93068
	}
	1266.1.1 = {
		 93066
	}
	1266.9.1 = {
		 93070
	}
	1271.1.1 = {
		 93072
	}
	1272.1.1 = {
		 93074
	}
	1282.1.1 = {
		 93075
	}
	1307.1.1 = {
		 93076
	}
	1308.1.1 = {
		 93078
	}
	1309.1.1 = {
		 93079
	}
	1310.1.1 = {
		 93080
	}
	1318.1.1 = {
		 93079
	}
	1326.1.1 = {
		 93081
	}
	1329.1.1 = {
		 93082
	}
	1330.1.1 = {
		 93083
	}
	1334.1.1 = {
		 93084
	}
	1335.1.1 = {
		 93086
	}
}


c_chitral = {
	867.1.1 = {	change_development_level = 3 }
	1066.1.1 = { change_development_level = 6 }
	
	750.1.1 = {
		liege = d_kabul
		 188565 # Surendravirkramaditya Nandin of Kabul Shahi
	}
	
	866.1.1 = {
		 175060 # Kallar Shahi
		government = clan_government	#Ahistorical, just for ease of bookmarks.
	}
	
	980.1.1 = {
		 188406 # Didda Lohara
		liege = k_kashmir
	}
	1003.1.1 = {
		 188409 # Samgramaraja
	}
	1028.1.1 = {
		 188414 # Ananta
	}
	1063.1.1 = {
		 188419 # Kalasa
	}
	1089.1.1 = {
		 188420 # Harsa
	}
	1101.1.1 = {
		 188418 # Uccala
	}
	1111.1.1 = {
		 188421 # Salhana
	}
	1112.1.1 = {
		 188422 # Sussala
	}
	1120.1.1 = {
		 188424 # Bhiksacara
	}
	1121.1.1 = {
		 188422 # Sussala again
	}
	1123.1.1 = {
		 188425 # Jayasimha
	}
	1155.1.1 = {
		 188426 # Paramanuka
	}
	1165.1.1 = {
		 188427 # Vantideva
	}
	1172.1.1 = {
		 188429 # Vuppadeva Vopyadeva
	}
	1181.1.1 = {
		 188430 # Jassaka
	}
	1199.1.1 = {
		 188431 # Jagadeva
	}
	1213.1.1 = {
		 188432 # Rajadeva
	}
	1236.1.1 = {
		 188433 # Samgramadeva
	}
	1252.1.1 = {
		 188434 # Ramadeva
	}
	1273.1.1 = {
		 188435 # Laksmadeva
	}
	1286.1.1 = {
		 188438 # Simhadeva
	}
	1301.1.1 = {
		 188439 # Suhadeva
	}
	1320.1.1 = {
		 188442 # Rincana
	}
	1323.1.1 = {
		 188444 # Udayanadeva
	}
}


d_zabulistan = {
	680.1.1 = {
		liege = 0
		 188701 # Zunbil founder
	}
	687.1.1 = {
		 188702
	}
	725.1.1 = {
		 188703
	}
	738.1.1 = {
		 188628
	}
	770.1.1 = {
		 188705
	}
	780.1.1 = {
		 188707
	}
	815.1.1 = {
		 188708
	}
	850.1.1 = {
		 188711 # Kabak Zunbil
	}
	865.1.1 = {
		 0 #Destroyed by the Saffarids
	}
	
	977.1.1 = {
		liege = k_kabulistan
		 144123 # Sabuktigin Ghaznavid
	}
	997.1.1 = {
		 144124 # Ismail
	}
	998.1.1 = {
		 144125 # Mahmud
	}
	1030.1.1 = {
		 144126 # Muhammad
	}
	1031.1.1 = {
		 144127 # Musud
	}
	1041.1.1 = {
		 144128 # Mawdud
	}
	1050.1.1 = {
		 144129 # Masud
	}
	1050.6.1 = {
		 144130 # Ali
	}
	1050.9.1 = {
		 144131 # Abd al-Rashid
	}
	1053.1.1 = {
		 144132 # Farrukhzad
	}
	1059.1.1 = {
		 144133 # Ibrahim
	}
	1099.1.1 = {
		 144134 # Masud II
	}
	1115.1.1 = {
		 144135 # Shirzad
	}
	1115.6.1 = {
		 144136 # Arlsan Shah
	}
	1118.1.1 = {
		liege = k_persia # Seljuk vassalage
		 144137 # Bahram Shah
	}
	1152.1.1 = {
		 144138 # Khusraw Shah
	}
	1157.5.8 = {
		liege = 0 # end of the Seljuk Turks
	}
	1160.1.1 = {
		 144139 # Khusraw Malik
	}
	1170.1.1 = {
		liege = 0
		 144141 # Ghiyath Ghurid
	}
	1173.1.1 = {
		liege = k_kabulistan
	}
	1186.1.1 = {
		liege = k_sindh
	}
	1203.1.1 = {
		 144142
		liege = e_rajastan
	}
	1206.3.15 = {
		 144143
		liege = 0
	}
	#Khwazerim
	#Kartids
	1212.1.1 = {
		liege = "k_persia"
		 93153
	}
	
	
	1221.1.1 = {
		liege = e_mongol_empire
	}
	1227.12.2 = {
		liege = "e_ilkhanate"
	}
	1245.1.1 = {
		 93154
	
	}
	1277.1.1 = {
		 93155
	}
	1295.1.1 = {
		 93156
	}
	1308.1.1 = {
		 93157
	}
	1330.1.1 = {
		 93158
	}
	1332.1.1 = {
		liege = 0
		 93159
	}
}

c_ghazna = {
	867.1.1 = {	change_development_level = 7 }
	1066.1.1 = { change_development_level = 10 }

	680.1.1 = {
		liege = d_sistan
		 188701 # Zunbil founder
	}
	687.1.1 = {
		 188702
	}
	725.1.1 = {
		 188703
	}
	738.1.1 = {
		 188628
	}
	770.1.1 = {
		 188705
	}
	780.1.1 = {
		 188707
	}
	815.1.1 = {
		 188708
	}
	850.1.1 = {
		 188711 # Kabak Zunbil
	}
	
	866.1.1 = {
		liege = d_kabul
		 175060 # Kallar Shahi
		government = clan_government	#Ahistorical, just for ease of bookmarks.
	}
	
	961.1.1 = {
		 170303 #Took Ghazna from Lawiks
	}
	975.1.1 = {
		 170304
	}
	977.1.1 = {
		 144123
	}
	997.1.1 = {
		 144124
	}
	998.1.1 = {
		 144125
	}
	1030.1.1 = {
		 144126
	}
	1031.1.1 = {
		 144127
	}
	1041.1.1 = {
		 144128
	}
	1050.1.1 = {
		 144129
	}
	1050.6.1 = {
		 144130
	}
	1050.9.1 = {
		 144131
	}
	1053.1.1 = {
		 144132
	}
	1059.1.1 = {
		 144133
	}
	1099.1.1 = {
		 144134
	}
	1115.1.1 = {
		 144135
	}
	1115.6.1 = {
		 144136
	}
	1118.1.1 = {
		 144137
	}
	1152.1.1 = {
		 144138
	}
	1160.1.1 = {
		 144139
	}
	1173.1.1 = {
		 144142
	}
	1206.3.15 = {
		 170302
	}
	1216.2.15 = {
		 93052
		liege = k_persia
	}
	1220.1.1 = {
		 194302
	}
}


c_zamindawar = {
	867.1.1 = {	change_development_level = 3 }
	1066.1.1 = { change_development_level = 6 }
	
	680.1.1 = {
		liege = d_sistan
		 188701 # Zunbil founder
	}
	687.1.1 = {
		 188702
	}
	725.1.1 = {
		 188703
	}
	738.1.1 = {
		 188628
	}
	770.1.1 = {
		 188705
	}
	780.1.1 = {
		 188707
	}
	815.1.1 = {
		 188708
	}
	850.1.1 = {
		 188711 # Kabak Zunbil
	}
	
	866.1.1 = {
		liege = d_kabul
		 175060 # Kallar Shahi
		government = clan_government	#Ahistorical, just for ease of bookmarks.
	}
	
	961.1.1 = {
		 170303 #Took Ghazna from Lawiks
	}
	975.1.1 = {
		 170304
	}
	977.1.1 = {
		 144123
	}
	997.1.1 = {
		 144124
	}
	998.1.1 = {
		 144125
	}
	1030.1.1 = {
		 144126
	}
	1031.1.1 = {
		 144127
	}
	1041.1.1 = {
		 144128
	}
	1050.1.1 = {
		 144129
	}
	1050.6.1 = {
		 144130
	}
	1050.9.1 = {
		 144131
	}
	1053.1.1 = {
		 144132
	}
	1059.1.1 = {
		 144133
	}
	1066.1.1 = {
		 20690
	}
	1099.1.1 = {
		 144134
	}
	1115.1.1 = {
		 144135
	}
	1115.6.1 = {
		 144136
	}
	1118.1.1 = {
		 144137
	}
	1152.1.1 = {
		 144138
	}
	1160.1.1 = {
		 144139
	}
	1173.1.1 = {
		 144142
	}
	1206.3.15 = {
		 170302
	}
	1216.2.15 = {
		 93052
		liege = k_persia
	}
	1220.1.1 = {
		 194302
	}
}


c_rukhaj = {
	680.1.1 = {
		liege = d_sistan
		 188701 # Zunbil founder
	}
	687.1.1 = {
		 188702
	}
	725.1.1 = {
		 188703
	}
	738.1.1 = {
		 188628
	}
	770.1.1 = {
		 188705
	}
	780.1.1 = {
		 188707
	}
	815.1.1 = {
		 188708
	}
	850.1.1 = {
		 188711 # Kabak Zunbil
	}
	
	866.1.1 = {
		liege = d_kabul
		 175060 # Kallar Shahi
		government = clan_government	#Ahistorical, just for ease of bookmarks.
	}
	
	961.1.1 = {
		 170303 #Took Ghazna from Lawiks
	}
	975.1.1 = {
		 170304
	}
	977.1.1 = {
		 144123
	}
	997.1.1 = {
		 144124
	}
	998.1.1 = {
		 144125
	}
	1030.1.1 = {
		 144126
	}
	1031.1.1 = {
		 144127
	}
	1041.1.1 = {
		 144128
	}
	1050.1.1 = {
		 144129
	}
	1050.6.1 = {
		 144130
	}
	1050.9.1 = {
		 144131
	}
	1053.1.1 = {
		 144132
	}
	1059.1.1 = {
		 144133
	}
	1066.1.1 = {
		 20690
	}
	1099.1.1 = {
		 144134
	}
	1115.1.1 = {
		 144135
	}
	1115.6.1 = {
		 144136
	}
	1118.1.1 = {
		 144137
	}
	1152.1.1 = {
		 144138
	}
	1160.1.1 = {
		 144139
	}
	1173.1.1 = {
		 144142
	}
	1206.3.15 = {
		 170302
	}
	1216.2.15 = {
		 93052
		liege = k_persia
	}
	1220.1.1 = {
		 194302
	}
}


c_zabulistan = {
	680.1.1 = {
		liege = d_zabulistan
		 188701 # Zunbil founder
	}
	687.1.1 = {
		 188702
	}
	725.1.1 = {
		 188703
	}
	738.1.1 = {
		 188628
	}
	770.1.1 = {
		 188705
	}
	780.1.1 = {
		 188707
	}
	815.1.1 = {
		 188708
	}
	850.1.1 = {
		 188711 # Kabak Zunbil
	}
	866.1.1 = {
		liege = d_kabul
		 175060 # Kallar Shahi
		government = clan_government	#Ahistorical, just for ease of bookmarks.
	}
	
	977.1.1 = {
		 144123 # Sabuktigin Ghaznavid
	}
	997.1.1 = {
		 144124 # Ismail
	}
	998.1.1 = {
		 144125 # Mahmud
	}
	1030.1.1 = {
		 144126 # Muhammad
	}
	1031.1.1 = {
		 144127 # Musud
	}
	1041.1.1 = {
		 144128 # Mawdud
	}
	1050.1.1 = {
		 144129 # Masud
	}
	1050.6.1 = {
		 144130 # Ali
	}
	1050.9.1 = {
		 144131 # Abd al-Rashid
	}
	1053.1.1 = {
		 144132 # Farrukhzad
	}
	1059.1.1 = {
		 144133 # Ibrahim
	}
	1099.1.1 = {
		 144134 # Masud II
	}
	1115.1.1 = {
		 144135 # Shirzad
	}
	1115.6.1 = {
		 144136 # Arlsan Shah
	}
	1118.1.1 = {
		liege = k_persia # Seljuk vassalage
		 144137 # Bahram Shah
	}
	1152.1.1 = {
		 144138 # Khusraw Shah
	}
	1157.5.8 = {
		liege = 0 # end of the Seljuk Turks
	}
	1160.1.1 = {
		 144139 # Khusraw Malik
	}
	1170.1.1 = {
		liege = 0
		 144141 # Ghiyath Ghurid
	}
	1173.1.1 = {
		liege = k_kabulistan
	}
	1186.1.1 = {
		liege = k_sindh
	}
	1203.1.1 = {
		 144142
		liege = e_rajastan
	}
	1206.3.15 = {
		 144143
		liege = 0
	}
	#Khwazerim
	#Kartids
	1212.1.1 = {
		liege = "k_persia"
		 93153
	}
	
	
	1221.1.1 = {
		liege = e_mongol_empire
	}
	1227.12.2 = {
		liege = "e_ilkhanate"
	}
	1245.1.1 = {
		 93154
	
	}
	1277.1.1 = {
		 93155
	}
	1295.1.1 = {
		 93156
	}
	1308.1.1 = {
		 93157
	}
	1330.1.1 = {
		 93158
	}
	1332.1.1 = {
		liege = 0
		 93159
	}
}
