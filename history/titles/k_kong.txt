
d_kong = {
	1065.1.1 = {
		 malinke0001
		government = tribal_government
	}
	1096.1.1 = {
		 malinke0002
	}
	1119.1.1 = {
		 malinke0003
	}
	1153.1.1 = {
		 malinke0004
	}
	1183.1.1 = {
		 malinke0005
	}
	1212.1.1 = {
		 malinke0006
	}
}
d_lobi = {
	867.1.1 = {	change_development_level = 1 }
	1066.1.1 = { change_development_level = 3 }

	860.1.1 = {
		 bobo0021
		government = tribal_government
	}
	890.1.1 = {
		 bobo0022
	}
	933.1.1 = {
		 bobo0023
	}
	990.1.1 = {
		 bobo0024
	}
	1050.1.1 = {
		 bobo0025
	}
	1081.1.1 = {
		 bobo0026
	}
	1120.1.1 = {
		 bobo0027
	}
	1159.1.1 = {
		 bobo0028
	}
	1199.1.1 = {
		 bobo0029
	}
	1235.1.1 = {
		 bobo0030
	}
}

c_worodugu = {
	867.1.1 = {	change_development_level = 3 }
	1066.1.1 = { change_development_level = 5 }

	860.1.1 = {
		 bobo0081
		government = tribal_government
	}
	912.1.1 = {
		 bobo0082
	}
	970.1.1 = {
		 bobo0083
	}
	1025.1.1 = {
		 bobo0084
	}
	1060.1.1 = {
		 bobo0085
	}
	1095.1.1 = {
		 bobo0086
	}
	1127.1.1 = {
		 bobo0087
	}
	1158.1.1 = {
		 bobo0088
	}
	1189.1.1 = {
		 bobo0089
	}
	1228.1.1 = {
		 bobo0090
	}
}


c_tyendugu = {
	867.1.1 = {	change_development_level = 1 }
	1066.1.1 = { change_development_level = 2 }

	860.1.1 = {
		 bobo0061
		government = tribal_government
	}
	890.1.1 = {
		 bobo0062
	}
	933.1.1 = {
		 bobo0063
	}
	990.1.1 = {
		 bobo0064
	}
	1050.1.1 = {
		 bobo0065
	}
	1081.1.1 = {
		 bobo0066
	}
	1120.1.1 = {
		 bobo0067
	}
	1159.1.1 = {
		 bobo0068
	}
	1199.1.1 = {
		 bobo0069
	}
	1235.1.1 = {
		 bobo0070
	}
}


c_nyene = {
	867.1.1 = {	change_development_level = 0 }
	1066.1.1 = { change_development_level = 1 }

	850.1.1 = {
		government = tribal_government
		 bobo0051
	}
	880.1.1 = {
		 bobo0052
	}
	902.1.1 = {
		 bobo0053
	}
	953.1.1 = {
		 bobo0054
	}
	1008.1.1 = {
		 bobo0055
	}
	1050.1.1 = {
		 bobo0056
	}
	1094.1.1 = {
		 bobo0057
	}
	1140.1.1 = {
		 bobo0058
	}
	1181.1.1 = {
		 bobo0059
	}
	1216.1.1 = {
		 bobo0060
	}
}


c_kong = {
	867.1.1 = {	change_development_level = 5 }
	1066.1.1 = { change_development_level = 8 }

	860.1.1 = {
		 bobo0071
		government = tribal_government
	}
	898.1.1 = {
		 bobo0072
	}
	950.1.1 = {
		 bobo0073
	}
	1000.1.1 = {
		 bobo0074
	}
	1035.1.1 = {
		 bobo0075
	}
	1065.1.1 = {
		liege = d_kong
		 malinke0001
		government = clan_government
	}
	1096.1.1 = {
		 malinke0002
	}
	1119.1.1 = {
		 malinke0003
	}
	1153.1.1 = {
		 malinke0004
	}
	1183.1.1 = {
		 malinke0005
	}
	1212.1.1 = {
		 malinke0006
	}
}


c_leraba = {
	867.1.1 = {	change_development_level = 0 }
	1066.1.1 = { change_development_level = 1 }

	860.1.1 = {
		 bobo0071
		government = tribal_government
	}
	898.1.1 = {
		 bobo0072
	}
	950.1.1 = {
		 bobo0073
	}
	1000.1.1 = {
		 bobo0074
	}
	1035.1.1 = {
		 bobo0075
	}
	1065.1.1 = {
		liege = d_kong
	}
	1070.1.1 = {
		 bobo0076
	}
	1098.1.1 = {
		 bobo0077
	}
	1133.1.1 = {
		 bobo0078
	}
	1163.1.1 = {
		 bobo0079
	}
	1195.1.1 = {
		 bobo0080
	}
}


c_boron = {
	867.1.1 = {	change_development_level = 0 }
	1066.1.1 = { change_development_level = 1 }

	860.1.1 = {
		 bobo0081
		government = tribal_government
	}
	912.1.1 = {
		 bobo0082
	}
	970.1.1 = {
		 bobo0083
	}
	1025.1.1 = {
		 bobo0084
	}
	1060.1.1 = {
		 bobo0085
	}
	1095.1.1 = {
		 bobo0086
	}
	1127.1.1 = {
		 bobo0087
	}
	1158.1.1 = {
		 bobo0088
	}
	1189.1.1 = {
		 bobo0089
	}
	1228.1.1 = {
		 bobo0090
	}
}


c_nzi = {
	867.1.1 = {	change_development_level = 0 }
	1066.1.1 = { change_development_level = 1 }

	860.1.1 = {
		government = tribal_government
	}
	
	860.1.1 = {
		 kru0011
		government = tribal_government
	}
	912.1.1 = {
		 kru0012
	}
	970.1.1 = {
		 kru0013
	}
	1025.1.1 = {
		 kru0014
	}
	1060.1.1 = {
		 kru0015
	}
	1095.1.1 = {
		 kru0016
	}
	1127.1.1 = {
		 kru0017
	}
	1158.1.1 = {
		 kru0018
	}
	1189.1.1 = {
		 kru0019
	}
	1228.1.1 = {
		 kru0020
	}
}


c_lobi = {
	860.1.1 = {
		 bobo0021
		liege = d_lobi
		government = tribal_government
	}
	890.1.1 = {
		 bobo0022
	}
	933.1.1 = {
		 bobo0023
	}
	990.1.1 = {
		 bobo0024
	}
	1050.1.1 = {
		 bobo0025
	}
	1081.1.1 = {
		 bobo0026
	}
	1120.1.1 = {
		 bobo0027
	}
	1159.1.1 = {
		 bobo0028
	}
	1199.1.1 = {
		 bobo0029
	}
	1235.1.1 = {
		 bobo0030
	}
}


c_loropeni = {
	867.1.1 = {	change_development_level = 0 }
	1066.1.1 = { change_development_level = 1 }

	850.1.1 = {
		government = tribal_government
		 bobo0041
	}
	893.1.1 = {
		 bobo0042
	}
	918.1.1 = {
		 bobo0043
	}
	968.1.1 = {
		 bobo0044
	}
	1025.1.1 = {
		 bobo0045
	}
	1066.1.1 = {
		liege = d_kong
		 bobo0046
	}
	1097.1.1 = {
		 bobo0047
	}
	1176.1.1 = {
		 bobo0048
	}
	1173.1.1 = {
		 bobo0049
	}
	1210.1.1 = {
		 bobo0050
	}
}


c_dyulaso = {
	867.1.1 = {	change_development_level = 0 }
	1066.1.1 = { change_development_level = 1 }

	860.1.1 = {
		 bobo0021
		liege = d_lobi
		government = tribal_government
	}
	890.1.1 = {
		 bobo0022
	}
	933.1.1 = {
		 bobo0023
	}
	990.1.1 = {
		 bobo0024
	}
	1050.1.1 = {
		 bobo0025
	}
	1081.1.1 = {
		 bobo0026
	}
	1120.1.1 = {
		 bobo0027
	}
	1159.1.1 = {
		 bobo0028
	}
	1199.1.1 = {
		 bobo0029
	}
	1235.1.1 = {
		 bobo0030
	}
}


c_wa = {
	867.1.1 = {	change_development_level = 0 }
	1066.1.1 = { change_development_level = 1 }

	860.1.1 = {
		government = tribal_government
	}
	
	860.1.1 = {
		 bobo0031
		liege = d_lobi
		government = tribal_government
	}
	899.1.1 = {
		 bobo0032
	}
	950.1.1 = {
		 bobo0033
	}
	1005.1.1 = {
		 bobo0034
	}
	1058.1.1 = {
		 bobo0035
	}
	1102.1.1 = {
		 bobo0036
	}
	1136.1.1 = {
		 bobo0037
	}
	1178.1.1 = {
		 bobo0038
	}
	1221.1.1 = {
		 bobo0039
	}
	1263.1.1 = {
		 bobo0040
	}
}

