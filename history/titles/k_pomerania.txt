
d_pommerania = {
	867.1.1 = { change_development_level = 2 }
	1066.1.1 = { change_development_level = 8 }
	
	#750.1.1 = {
	#	 194071
	#}
	#799.1.1 = {
	#	 194072
	#}
	#822.1.1 = {
	#	 194073
	#}
	#867.1.1 = {
	# 184004
	#}
	
	1066.1.1 = {
		 30522 #Siemomysl
	}
	1068.1.1 = {
		 30523 #Swantibor
	}
	1092.1.1 = {
		 30524 #Gwienomir
	}
	1106.1.1 = {
		 221703 #Swietopelk I
	}
	1122.1.1 = {
		 221700 #Wartislaw I
	}
	1124.1.1 = {
		liege = "k_poland"
	}
	1136.1.1 = {
		liege = 0
	}
	1136.1.1 = {
		 221718 #Ratibor I
	}
	1156.5.7 = {
		 221710 #Bogislaw I
	}
	1168.1.1 = {
		liege = "k_denmark"
	}
	1187.3.18 = {
		 221712 #Bogislaw II
	}
	1220.4.1 = {
		 451953 #Barnim I
	}
	#battle of Bornhoved
	1227.7.22 = {
		liege = "e_hre"
	}
	1278.2.14 = {
		 471660 #Bogislaw III
	}
	1309.3.2 = {
		 471650 #Wartislaw II
	}
	1326.8.1 = {
		 471655 #Bogislaw IV
	}
	
	

}


c_stettin = {
	867.1.1 = { change_development_level = 4 }
	1066.1.1 = { change_development_level = 12 }
	
	###867 Pomeranian tribes
	500.1.1 = {
		liege = 0

		government = tribal_government
	}
	867.1.1 = {
		 302070
	}
	
	###1066
	1066.1.1 = {
		 30522 #Siemomysl
	liege = d_pommerania
	government = feudal_government
	}
	1068.1.1 = {
		 30523 #Swantibor
	}
	1092.1.1 = {
		 30524 #Gwienomir
	}
	1106.1.1 = {
		 221703 #Swietopelk I
	}
	1122.1.1 = {
		 221700 #Wartislaw I
	}
	1136.1.1 = {
		 221710 #Bogislaw I
	}
	1187.3.18 = {
		 221712 #Bogislaw II
	}
	1220.4.1 = {
		 451953 #Barnim I
	}
	1278.2.14 = {
		 471660 #Bogislaw
	}
	1294.1.1 = {
		 451950 #Otton
	}
	
	
	
	

}


c_kolobrzeg = {
	867.1.1 = { change_development_level = 2 }
	1066.1.1 = { change_development_level = 10 }
	
	###867 Pommeranian tribes
	500.1.1 = {
		liege = 0
	}
	867.1.1 = {
		 302070
	}
	
	###1066
	1066.1.1 = {
		 30522 #Siemomysl
	liege = d_pommerania
	}
	1068.1.1 = {
		 30523 #Swantibor
	}
	1092.1.1 = {
		 30524 #Gwienomir
	}
	1106.1.1 = {
		 221703 #Swietopelk I
	}
	1122.1.1 = {
		 221700 #Wartislaw I
	}
	1136.1.1 = {
		 221710 #Bogislaw I
	}
	1187.3.18 = {
		 221712 #Bogislaw II
	}
	1220.4.1 = {
		 451953 #Barnim I
	}
	1278.2.14 = {
		 471660 #Bogislaw
	}
	1294.1.1 = {
		 451950 #Otton
	}
	
	
	
	

}


c_soldin = {
	###867 Pommeranian tribes
	500.1.1 = {
		liege = 0

		government = tribal_government
	}
	867.1.1 = {
		 302071		#Lucjan Pyritzi
	}
	
	###1066
	1066.1.1 = {
		liege = d_pommerania
		 30524
		government = feudal_government
	}
	1068.1.1 = {
		 30523 #Swantibor
	}
	1092.1.1 = {
		 30524 #Gwienomir
	}
	1106.1.1 = {
		 221703 #Swietopelk I
	}
	1122.1.1 = {
		 221700 #Wartislaw I
	}
	1136.1.1 = {
		 221710 #Bogislaw I
	}
	1187.3.18 = {
		 221712 #Bogislaw II
	}
	1220.4.1 = {
		 451953 #Barnim I
	}
	1278.2.14 = {
		 471660 #Bogislaw
	}
	1294.1.1 = {
		 451950 #Otton
	}
	
	
	
	

}


c_cammin = {
	867.1.1 = { change_development_level = 2 }
	1066.1.1 = { change_development_level = 10 }
	
	###867 Pommeranian tribes
	500.1.1 = {
		liege = 0
	}
	867.1.1 = {
		 302070
	}
	
	###1066
	1066.1.1 = {
		 30522 #Siemomysl
	liege = d_pommerania
	}
	1068.1.1 = {
		 30523 #Swantibor
	}
	1092.1.1 = {
		 30524 #Gwienomir
	}
	
	1106.1.1 = {
		 221703 #Swietopelk I
	}
	1122.1.1 = {
		 221718 #Ratibor I
	}
	1156.5.7 = {
		 221723 #Swietopelk II
	}
	1175.1.20 = {
		 221719 #Wratislaw I
	}
	1182.1.1 = {
		 221720 #Bogislaw
	}
	1224.1.1 = {
		 221721 #Ratibor II
	}
	
	
	1228.1.1 = {
		liege = "d_pomerelia"
	}
	1228.1.1 = {
		 30452 #Wartislaw I
	}
	1233.5.4 = {
		 30451 #Swietopelk II
	}
	1266.1.10 = {
		 30453 #Sambor II
	}
	1270.1.1 = {
		 30459 #Wartislaw II
	}
	1271.1.1 = {
		 30458 #Mszczuj II
	}
	
	1294.12.25 = {
		 30538  #Swenzo
	}
	
	1308.1.1 = {
		 30540  #Piotr
	}
	1308.11.13 = {
		liege = "d_pommerania"
	}
	1327.1.1 = {
		 30541 #Jasko
	}
}


c_pila = {
	###867 Pommeranian tribes
	500.1.1 = {
		liege = 0

		government = tribal_government
	}
	867.1.1 = {
		 302071		#Lucjan Pyritzi
	}
	
	###1066
	1066.1.1 = {
		liege = d_pommerania
		 30524
		government = feudal_government
	}
	1106.1.1 = {
		 221703 #Swietopelk I
	}
	1122.1.1 = {
		 221700 #Wartislaw I
	}
	1136.1.1 = {
		 221710 #Bogislaw I
	}
	1187.3.18 = {
		 221712 #Bogislaw II
	}
	1220.4.1 = {
		 451953 #Barnim I
	}
	1278.2.14 = {
		 471660 #Bogislaw
	}
	1294.1.1 = {
		 451950 #Otton
	}
	
	
	
	

}


c_landsberg = {
	###867 Pommeranian tribes
	500.1.1 = {
		liege = 0

		government = tribal_government
	}
	867.1.1 = {
		 302071		#Lucjan Pyritzi
	}
	
	###1066
	1066.1.1 = {
		liege = d_pommerania
		 30524
		government = feudal_government
	}
	1068.1.1 = {
		 30523 #Swantibor
	}
	1092.1.1 = {
		 30524 #Gwienomir
	}
	1106.1.1 = {
		 221703 #Swietopelk I
	}
	1122.1.1 = {
		 221700 #Wartislaw I
	}
	1136.1.1 = {
		 221710 #Bogislaw I
	}
	1187.3.18 = {
		 221712 #Bogislaw II
	}
	1220.4.1 = {
		 451953 #Barnim I
	}
	1278.2.14 = {
		 471660 #Bogislaw
	}
	1294.1.1 = {
		 451950 #Otton
	}
	
	
	
	

}


d_nordmark = {
	867.1.1 = { change_development_level = 2 }
	1066.1.1 = { change_development_level = 5 }
	
	#750.1.1 = {
	#	 194062
	#}
	#799.1.1 = {
	#	 194063
	#}
	#822.1.1 = {
	#	 194064
	#}
	#867.1.1 = {
	#	 184001 #Msciwoj
	#}
	954.1.1 = {
		 161118 #Nakon Nakonid
	}
	966.1.1 = {
		 161119 #Mistivoj Nakonid
	}
	995.1.1 = {
		 161121  #Mscislaw Nakonid
	}
	1005.1.1 = {
		 8411 #Pribigniew Nakonid
	}
	1028.1.1 = {
		 20586 #Gryn Wizlawid
	}
	1043.1.1 = {
		 8410 #Gottschalk Nakonid
	}
	1066.6.7 = {
		 20584 #Kruto Wizlawid
	}
	1093.1.1 = {
		 8402 #Henrik Nakonid
	}
	1127.6.7 = {
		 30509 #Kanut Nakonid
	}
	1128.1.1 = {
		 30508 #Swietopelk Nakonid
	}
	1129.1.1 = {
		 30512 #Swineke Nakonid
	}
	1129.7.1 = {
		 201513 #Knud Lavard
		liege = "e_hre"
	}
	1131.1.13 = {
		liege = 0
	}
	1131.1.13 = {
		 452005 #Niklot
	}
	1160.8.2 = {
		liege = "e_hre"
	}
	1160.8.2 = {
		 452004 #Przybyslaw Niklotid
	}
	1168.1.1 = {
		liege = "k_denmark"
	}
	1178.12.1 = {
		 452003 #Heinrich Borwin I
	}
	1227.1.28 = {
		 452001 #Johann I Niklotid
	}
	1227.7.22 = {
		liege = "e_hre"
	}
	1264.8.1 = {
		 452000 #Heinrich I Niklotid
	}
	1302.1.19 = {
		 451981 #Heinrich II Niklotid
	}
	1329.1.27 = {
		 451980 #Albrecht I Niklotid
	}
}


c_mecklenburg = {
	###867 Polabian tribes
	837.1.1 = {
		liege = 0
		 194064

		government = tribal_government
	}
	866.1.1 = {
		 184001
	}
	935.1.1 = {
		government = feudal_government	#Ahistorical, just for ease of bookmarks.
	}
	
	#1066
	1043.1.1 = {
		 8410 #Gottschalk Nakonid
	liege = d_nordmark
	}
	1066.6.7 = {
		 20584 #Kruto Wizlawid
	}
	1093.1.1 = {
		 8402 #Henrik Nakonid
	}
	1127.6.7 = {
		 30509 #Kanut Nakonid
	}
	1128.1.1 = {
		 30508 #Swietopelk Nakonid
	}
	1129.1.1 = {
		 30512 #Swineke Nakonid
	}
	1129.7.1 = {
		 201513 #Knud Lavard
	}
	1131.1.13 = {
		 452005 #Niklot
	}
	
	1160.8.2 = {
		 452004 #Przybyslaw Niklotid
	}
	
	1178.12.1 = {
		 452003 #Heinrich Borwin I
	}
	
	1227.1.28 = {
		 452001 #Johann I Niklotid
	}
	
	1264.8.1 =
	
	{
	 452000 #Heinrich I Niklotid
	}
	
	1302.1.19 = {
		 451981 #Heinrich II Niklotid
	}
	1329.1.27 = {
		 451980 #Albrecht I Niklotid
	}
}


b_ratzeburg = {
	1202.1.1 = {
		 31795 #Albrecht Askanien
	}
	1227.7.22 = {
		 462114
	}
	1260.10.7 = {
		 33336
	}
	1282.1.1 = {
		 462101
	}
	
	#duchy of Saxe-Ratzeburg-Lauenburg
	1296.1.1 = {
		 33343
	}
	1308.1.1 = {
		 33341
	}
	1322.4.22 = {
		 33344
	}

}


b_schwerin = {
	1167.1.1 = {
		 33265
	}
	1185.6.18 = {
		 33266
	}
	1220.5.25 = {
		 33269
	}
	1247.1.1 = {
		 33271
	}
	1297.1.1 = {
		 33277
	}
	1323.6.23 = {
		 33278
	}
	1327.1.1 = {
		 33287
	}
	
	
	
	
	
	
	

}


c_wolgast = {
	###867 Pommeranian tribes
	500.1.1 = {
		liege = 0

		government = tribal_government
	}
	867.1.1 = {
		 302072
	}
	
	###1043.1.1
	1043.1.1 = {
		liege = "d_nordmark"
	government = feudal_government
	}
	
	1043.1.1 = {
		 20584 #Kruto Wizlawid
	}
	1093.1.1 = {
		 30481 #Wartislaw Wizlawid
	}
	
	#Conquest by Wartislaw I of Pommerania
	
	1122.1.1 = {
		liege = "d_pommerania"
	}
	1122.1.1 = {
		 221700 #Wartislaw I
	}
	1136.1.1 = {
		 221718 #Ratibor I
	}
	1156.5.7 = {
		 221715 #Kasimir
	}
	1180.11.1 = {
		 221717 #Ratibor
	}
	1183.1.14 = {
		 221712 #Bogislaw II
	}
	1220.4.1 = {
		 451953 #Barnim I
	}
	1278.2.14 = {
		 471660 #Bogislaw III
	}
	1309.3.2 = {
		 471650 #Wartislaw II
	}
	1326.8.1 = {
		 471655 #Bogislaw
	}
}


c_rugen = {
	###867 Pommeranian tribes
	500.1.1 = {
		liege = 0

		government = tribal_government
	}
	867.1.1 = {
		 302073
	}
	
	###1043
	1043.1.1 = {
		liege = "d_nordmark"

		government = feudal_government
	}
	
	1043.1.1 = {
		 20584 #Kruto Wizlawid
	}
	1066.1.1 = {
		 8420
	}
	1093.1.1 = {
		liege = 0
	}
	1093.1.1 = {
		 30481 #Wartislaw Wizlawid
	}
	1125.1.1 = {
		 30482 #Rostislav Wizlawid
	}
	1141.1.1 = {
		 221734 #Wizlaw Wizlawid
	}
	1163.1.1 = {
		 221730 #Testislaw
	}
	1168.1.1 = {
		liege = "d_skane"
	}
	1170.1.1 = {
		 221735 #Jaromar I
	}
	1218.1.1 = {
		 221736 #Barnuta
	}
	1221.1.1 = {
		 221737 #Wizlaw I
	}
	
	1256.1.1 = {
		 30491 #Jaromar II
	}
	
	1260.8.20 = {
		 30496 #Jaromar III
	}
	1285.1.1 = {
		 30497 #Wizlaw II
	}
	1302.12.29 = {
		 30498 #Wizlaw III
	}
	
	1325.11.8 = {
		liege = "d_pommerania"
	}
	1325.11.8 = {
		 471650 #Wartislaw II
	}
	1326.8.1 = {
		 471653 #Barnim
	}

}


c_rostock = {
	###867 Pommeranian tribes
	500.1.1 = {
		liege = 0
	}
	867.1.1 = {
		 302073
	}
	
	###1043
	1043.1.1 = {
		 8410 #Gottschalk Nakonid
	}
	1066.6.7 = {
		 20584 #Kruto Wizlawid
	}
	1093.1.1 = {
		 8402 #Henrik Nakonid
	}
	1127.6.7 = {
		 30508 #Swietopelk Nakonid
	}
	1129.1.1 = {
		 30512 #Swineke Nakonid
	}
	1129.7.1 = {
		 30507 #Przybyslaw Nakonid
	}
	1131.1.13 = {
		 452005 #Niklot
	}
	1160.8.2 = {
		 30550 #Wartislaw Niklotid
	}
	1165.1.1 = {
		 30551 #Prislaw Niklotid
	}
	1175.1.1 = {
		 30553 #Knud Niklotid
	}
	1183.11.6 = {
		 30552 #Niklot Niklotid
	}
	1200.5.6 = {
		 452002 #Heinrich Borwin I
	}
	1226.12.5 = {
		 30560 #Heinrich Borwin III
	}
	1277.12.2 =
	
	{
	 30587 #Waldemar Niklotid
	}
	1282.11.9 = {
		 30591 #Nikolaus Niklotid
	}
	1314.11.25 = {
		 30576 #Johann Niklotid
	}
}


c_werle = {
	###867 Pommeranian tribes
	500.1.1 = {
		liege = 0
	}
	867.1.1 = {
		 302072
	}
	
	#Lutici rebel against the HRE, become independant in ~985
	##Gave Lutician territory to a single dude with no vassal to make their life easier against their behemoths of neighbours HRE & Poland
	983.1.1 = {
		liege = 0
		 303438	#Kornel Lutici
	}
	1012.11.4 = {
		 303439	#Marek Lutici
	}
	#1035 Lutici become tributaries to the HRE
	1035.1.24 = {
		government = feudal_government
		 303440	#Kornel Lutici
	}
	1064.9.2 = {
		 303442	#Kasper Lutici
	}
	1091.12.4 = {
		 303446	#Kornel Lutici
	}
	
	#XIIth - "Luticia" partitioned between their neighbours (HRE & Obotrites)
	1123.1.1 = {
		liege = e_hre
		 30481 #Wartislaw Wizlawid
	}
	1125.1.1 = {
		 30482 #Rostislav Wizlawid
	}
	1141.1.1 = {
		 221734 #Wizlaw Wizlawid
	}
	1163.1.1 = {
		 221730 #Testislaw Wizlawid
	}
	
	1168.1.1 = {
		liege = "d_skane"
	}
	1170.1.1 = {
		 30484 #Dubizlaw Wizlawid
	}
	
	1173.1.1 = {
		liege = "d_nordmark"
	}
	1173.1.1 = {
		 452003 #Heinrich Borwin I
	}
	1227.1.28 = {
		 452007 #Nikolaus Niklotid
	}
	1277.5.14 = {
		 452006 #Johann Niklotid
	}
	1283.10.15 = {
		 451991 #Nikolaus Niklotid
	}
	1316.10.12 = {
		 451990 #Johann Niklotid
	}
}


d_pomerelia = {
	867.1.1 = { change_development_level = 2 }
	1066.1.1 = { change_development_level = 8 }
	
	1155.1.1 = {
		liege = "k_poland"
	}
	1155.1.1 = {
		 221760 #Subislaw I
	}
	1187.1.29 = {
		 221750 #Sambor I
	}
	1207.2.7 = {
		 221755 #Mszczuj I
	}
	1210.1.1 = {
		liege = "k_denmark"
	}
	1212.1.1 = {
		liege = "k_poland"
	}
	1219.5.1 = {
		 221751 #Subislaw II
	}
	1223.12.28 = {
		 30450 #Swietopelk I
	}
	1227.1.1 = {
		 30452 #Wartislaw I
	}
	1233.5.4 = {
		 30451 #Swietopelk II
	}
	1266.1.10 = {
		 30453 #Sambor II
	}
	1270.1.1 = {
		 30459 #Wartislaw II
	}
	1271.1.1 = {
		 30458 #Mszczuj II
	}
	1294.12.25 = {
		 125977 #Przemyslaw Piast
	}
	1296.2.8 = {
		 471510 #Wladyslaw Piast
	}
	1308.11.13 = {
		 0
	}
}


c_danzig = {
	867.1.1 = { change_development_level = 3 }
	1066.1.1 = { change_development_level = 11 }
	
	###867 tribal times
	500.1.1 = {
		liege = 0
	}
	860.1.1 = {
		 302068
	}
	
	###1066
	1066.1.1 = {
		liege = d_pommerania
		 30523
	}
	1092.1.1 = {
		 30524
	}
	1106.1.1 = {
		 221703
	}
	1122.1.1 = {
		 221700
	}
	1136.1.1 = {
		 221718 #Ratibor I
	}
	1155.1.1 = {
		liege = "d_pomerelia"
	}
	1155.1.1 = {
		 221760 #Subislaw I
	}
	1187.1.29 = {
		 221750 #Sambor I
	}
	1207.2.7 = {
		 221755 #Mszczuj I
	}
	1219.5.1 = {
		 221751 #Subislaw II
	}
	1223.12.28 = {
		 30450 #Swietopelk I
	}
	1227.1.1 = {
		 30452 #Wartislaw I
	}
	1233.5.4 = {
		 30451 #Swietopelk II
	}
	1266.1.10 = {
		 30453 #Sambor II
	}
	1270.1.1 = {
		 30459 #Wartislaw II
	}
	1271.1.1 = {
		 30458 #Mszczuj II
	}
	1294.12.25 = {
		 125977 #Przemyslaw Piast
	}
	1296.2.8 = {
		 471510 #Wladyslaw Piast
	}
	
	###Teutonic Order
	
	1308.11.13 = {
		liege = "d_teutonic_order"
	}
	1308.11.13 = {
		 30414 #Siegfried von Feuchtwangen
	}
	1311.5.3 = {
		 30415 #Karl von Trier
	}
	1324.2.11 = {
		 30416 #Werner von Orseln
	}
	1330.11.18 = {
		 30417 #Luther Welf
	}
	1335.4.18 = {
		 474000 #Dietrich von Altenburg
	}
	

}


c_slupsk = {
	###867 Pommeranian tribes
	500.1.1 = {
		liege = 0

		government = tribal_government
	}
	867.1.1 = {
		 302069		#Jaromil Slovinci
	}
	
	###1066
	1066.1.1 = {
		liege = d_pommerania
	 30523
	government = feudal_government
	}
	1092.1.1 = {
		 30524 #Gwienomir
	}
	
	1106.1.1 = {
		 221703 #Swietopelk I
	}
	1122.1.1 = {
		 221718 #Ratibor I
	}
	1156.5.7 = {
		 221723 #Swietopelk II
	}
	1175.1.20 = {
		 221719 #Wratislaw I
	}
	1182.1.1 = {
		 221720 #Bogislaw
	}
	1224.1.1 = {
		 221721 #Ratibor II
	}
	
	
	1228.1.1 = {
		liege = "d_pomerelia"
	}
	1228.1.1 = {
		 30452 #Wartislaw I
	}
	1233.5.4 = {
		 30451 #Swietopelk II
	}
	1266.1.10 = {
		 30453 #Sambor II
	}
	1270.1.1 = {
		 30459 #Wartislaw II
	}
	1271.1.1 = {
		 30458 #Mszczuj II
	}
	
	1294.12.25 = {
		 30538  #Swenzo
	}
	
	1308.1.1 = {
		 30540  #Piotr
	}
	1308.11.13 = {
		liege = "d_pommerania"
	}
	1327.1.1 = {
		 30541 #Jasko
	}
}


c_miastko = {
	###867 Pommeranian tribes
	500.1.1 = {
		liege = 0
	}
	867.1.1 = {
		 302069		#Jaromil Slovinci
	}
	
	###1066
	1066.1.1 = {
		liege = d_pommerania
		 30523
	}
	1092.1.1 = {
		 30524 #Gwienomir
	}
	
	1106.1.1 = {
		 221703 #Swietopelk I
	}
	1122.1.1 = {
		 221718 #Ratibor I
	}
	1156.5.7 = {
		 221723 #Swietopelk II
	}
	1175.1.20 = {
		 221719 #Wratislaw I
	}
	1182.1.1 = {
		 221720 #Bogislaw
	}
	1224.1.1 = {
		 221721 #Ratibor II
	}
	
	
	1228.1.1 = {
		liege = "d_pomerelia"
	}
	1228.1.1 = {
		 30452 #Wartislaw I
	}
	1233.5.4 = {
		 30451 #Swietopelk II
	}
	1266.1.10 = {
		 30453 #Sambor II
	}
	1270.1.1 = {
		 30459 #Wartislaw II
	}
	1271.1.1 = {
		 30458 #Mszczuj II
	}
	
	1294.12.25 = {
		 30538  #Swenzo
	}
	
	1308.1.1 = {
		 30540  #Piotr
	}
	1308.11.13 = {
		liege = "d_pommerania"
	}
	1327.1.1 = {
		 30541 #Jasko
	}
}


c_berent = {
	###867 tribal times
	500.1.1 = {
		liege = 0
	}
	860.1.1 = {
		 302068
	}
	
	###1066
	1066.1.1 = {
		 30523
	liege = d_pommerania
	}
	1092.1.1 = {
		 30524 #Gwienomir
	}
	
	1106.1.1 = {
		 221703 #Swietopelk I
	}
	1122.1.1 = {
		 221718 #Ratibor I
	}
	1156.5.7 = {
		 221723 #Swietopelk II
	}
	1175.1.20 = {
		 221719 #Wratislaw I
	}
	1182.1.1 = {
		 221720 #Bogislaw
	}
	1224.1.1 = {
		 221721 #Ratibor II
	}
	
	
	1228.1.1 = {
		liege = "d_pomerelia"
	}
	1228.1.1 = {
		 30452 #Wartislaw I
	}
	1233.5.4 = {
		 30451 #Swietopelk II
	}
	1266.1.10 = {
		 30453 #Sambor II
	}
	1270.1.1 = {
		 30459 #Wartislaw II
	}
	1271.1.1 = {
		 30458 #Mszczuj II
	}
	
	1294.12.25 = {
		 30538  #Swenzo
	}
	
	1308.1.1 = {
		 30540  #Piotr
	}
	1308.11.13 = {
		liege = "d_pommerania"
	}
	1327.1.1 = {
		 30541 #Jasko
	}
}


d_ostmark = {
	867.1.1 = { change_development_level = 2 }
	1066.1.1 = { change_development_level = 8 }
	
	#768.1.1 = {
	#	 168600
	#}
	#806.1.1 = {
	#	 168601
	#}
	867.1.1 = {
		 168582
		government = tribal_government
	}
	
	#Geronen
	926.1.1 = {
		liege = 0
		 168562
	}
	965.5.20 = {
		 168568
	}
	979.8.3 = {
		 168573
	}
	981.1.1 = {
	}
	
	#Lutici rebel against the HRE, become independant in ~985
	##Gave Lutician territory to a single dude with no vassal to make their life easier against their behemoths of neighbours HRE & Poland
	983.1.1 = {
		liege = 0
		 303438	#Kornel Lutici
	}
	1012.11.4 = {
		 303439	#Marek Lutici
	}
	#1035 Lutici become tributaries to the HRE
	1035.1.24 = {
		government = feudal_government
		 303440	#Kornel Lutici
	}
	1064.9.2 = {
		 303442	#Kasper Lutici
	}
	1091.12.4 = {
		 303446	#Kornel Lutici
	}
	
	#XIIth - "Luticia" partitioned between their neighbours (HRE & Obotrites)
	1123.1.1 = {
		liege = e_hre
		 33788
	}
	1124.5.22 = {
		 212908
	}
	1131.1.1 = {
		 33790
	}
	1135.12.31 = {
		 212040
	}
	1157.2.5 = {
		 212048
	}
	1185.2.9 = {
		 212938
	}
	1190.8.16 = {
		 212550
	}
	1210.5.6 = {
		 212042
	}
	1221.2.17 = {
		 7504
	}
	1288.2.15 = {
		 7509
	}
	1291.8.16 = {
		 33427
	}
	1307.12.10 = {
		 33758
	}
	1308.12.1 = {
		 7560
	}
	1320.8.20 = {
		 462731
	}
}


c_brandenburg = {
	#867 Polabian tribes
	768.1.1 = {
		 168600
		liege = 0

		government = tribal_government
	}
	806.1.1 = {
		 168601
	}
	867.1.1 = {
		 168582
	}
	920.1.1 = {
		 168554
	}
	
	#Germany
	960.1.1 = {
		liege = d_ostmark
		 168555
	}
	
	#Lutici rebel against the HRE, become independant in ~985
	##Gave Lutician territory to a single dude with no vassal to make their life easier against their behemoths of neighbours HRE & Poland
	983.1.1 = {
		liege = 0
		 303438	#Kornel Lutici
	}
	1012.11.4 = {
		 303439	#Marek Lutici
	}
	#1035 Lutici become tributaries to the HRE
	1035.1.24 = {
		government = feudal_government
		 303440	#Kornel Lutici
	}
	1064.9.2 = {
		 303442	#Kasper Lutici
	}
	1091.12.4 = {
		 303446	#Kornel Lutici
	}
	
	#XIIth - "Luticia" partitioned between their neighbours (HRE & Obotrites)
	1123.1.1 = {
		liege = e_hre
		 30231 #Heinrich II
	}
	1128.12.4 = {
		 30232 #Udo IV
	}
	
	1130.3.15 = {
		 30262 #Konrad von Plotzkau
	}
	
	1133.1.10 = {
		 30233 #Rudolf II
	}
	1144.1.1 = {
		 212908
	}
	1170.11.18 = {
		 212030
	}
	1184.7.8 = {
		 212037
	}
	1205.7.4 = {
		 212032
	}
	1220.2.25 = {
		 7688
	}
	1267.2.2 = {
		 7558
	}
	1318.2.14 = {
		 7560
	}
	1320.8.20 = {
		 462731
	}

}


c_ruppin = {
	#867 Polabian tribes
	768.1.1 = {
		 168600
		liege = 0

		government = tribal_government
	}
	806.1.1 = {
		 168601
	}
	867.1.1 = {
		 168582
	}
	920.1.1 = {
		 168554
	}
	
	#Germany
	960.1.1 = {
		liege = d_ostmark
		 168555
	}
	
	#Lutici rebel against the HRE, become independant in ~985
	##Gave Lutician territory to a single dude with no vassal to make their life easier against their behemoths of neighbours HRE & Poland
	983.1.1 = {
		liege = 0
		 303438	#Kornel Lutici
	}
	1012.11.4 = {
		 303439	#Marek Lutici
	}
	#1035 Lutici become tributaries to the HRE
	1035.1.24 = {
		government = feudal_government
		 303440	#Kornel Lutici
	}
	1064.9.2 = {
		 303442	#Kasper Lutici
	}
	1091.12.4 = {
		 303446	#Kornel Lutici
	}
	
	#XIIth - "Luticia" partitioned between their neighbours (HRE & Obotrites)
	1123.1.1 = {
		liege = e_hre
		 30231 #Heinrich II
	}
	
	1128.12.4 = {
		 30232 #Udo IV
	}
	
	1130.3.15 = {
		 30262 #Konrad von Plotzkau
	}
	
	1133.1.10 = {
		 30233 #Rudolf II
	}
	1144.1.1 = {
		 212908
	}
	1170.11.18 = {
		 212030
	}
	1184.7.8 = {
		 212037
	}
	1205.7.4 = {
		 212032
	}
	1220.2.25 = {
		 7688
	}
	1267.2.2 = {
		 7558
	}
	1318.2.14 = {
		 7560
	}
	1320.8.20 = {
		 462731
	}

}


c_berlin = {
	#867 Polabian tribes
	860.1.1 = {
		liege = 0
		 303435	#fictional Walenty Redari

		government = tribal_government
	}
	
	#Germany
	960.1.1 = {
		liege = d_ostmark
		 168555
	}
	
	#Lutici rebel against the HRE, become independant in ~985
	##Gave Lutician territory to a single dude with no vassal to make their life easier against their behemoths of neighbours HRE & Poland
	983.1.1 = {
		liege = 0
		 303438	#Kornel Lutici
	}
	1012.11.4 = {
		 303439	#Marek Lutici
	}
	#1035 Lutici become tributaries to the HRE
	1035.1.24 = {
		government = feudal_government
		 303440	#Kornel Lutici
	}
	1064.9.2 = {
		 303442	#Kasper Lutici
	}
	1091.12.4 = {
		 303446	#Kornel Lutici
	}
	
	#XIIth - "Luticia" partitioned between their neighbours (HRE & Obotrites)
	1123.1.1 = {
		liege = e_hre
		 30231 #Heinrich II
	}
	
	1128.12.4 = {
		 30232 #Udo IV
	}
	
	1130.3.15 = {
		 30262 #Konrad von Plotzkau
	}
	
	1133.1.10 = {
		 30233 #Rudolf II
	}
	1144.1.1 = {
		 212908
	}
	1170.11.18 = {
		 212030
	}
	1184.7.8 = {
		 212037
	}
	1205.7.4 = {
		 212032
	}
	1220.2.25 = {
		 7688
	}
	1267.2.2 = {
		 7558
	}
	1318.2.14 = {
		 7560
	}
	1320.8.20 = {
		 462731
	}

}


c_havelberg = {
	#867 Polabian tribes
	768.1.1 = {
		 168600
		liege = 0
	}
	806.1.1 = {
		 168601
	}
	867.1.1 = {
		 168582
	}
	920.1.1 = {
		 168554
	}
	
	#Germany
	960.1.1 = {
		liege = d_ostmark
		 168555
	}
	
	#Lutici rebel against the HRE, become independant in ~985
	##Gave Lutician territory to a single dude with no vassal to make their life easier against their behemoths of neighbours HRE & Poland
	983.1.1 = {
		liege = 0
		 303438	#Kornel Lutici
	}
	1012.11.4 = {
		 303439	#Marek Lutici
	}
	#1035 Lutici become tributaries to the HRE
	1035.1.24 = {
		government = feudal_government
		 303440	#Kornel Lutici
	}
	1064.9.2 = {
		 303442	#Kasper Lutici
	}
	1091.12.4 = {
		 303446	#Kornel Lutici
	}
	
	#XIIth - "Luticia" partitioned between their neighbours (HRE & Obotrites)
	1123.1.1 = {
		liege = e_hre
		 30231 #Heinrich II
	}
	1128.12.4 = {
		 30232 #Udo IV
	}
	
	1130.3.15 = {
		 30262 #Konrad von Plotzkau
	}
	
	1133.1.10 = {
		 30233 #Rudolf II
	}
	1144.1.1 = {
		 212908
	}
	1170.11.18 = {
		 212030
	}
	1184.7.8 = {
		 212037
	}
	1205.7.4 = {
		 212032
	}
	1220.2.25 = {
		 7688
	}
	1267.2.2 = {
		 7558
	}
	1318.2.14 = {
		 7560
	}
	1320.8.20 = {
		 462731
	}

}


c_prenzlau = {
	#867 Polabian tribes
	865.1.1 = {
		liege = 0
		 303435	#fictional Walenty Redari

		government = tribal_government
	}
	
	#1066
	1066.1.1 = {
		 30522 #Siemomysl
	liege = d_pommerania

		government = feudal_government
	}
	1068.1.1 = {
		 30523 #Swantibor
	}
	1092.1.1 = {
		 30524 #Gwienomir
	}
	1106.1.1 = {
		 221703 #Swietopelk I
	}
	1122.1.1 = {
		 221700 #Wartislaw I
	}
	1136.1.1 = {
		 221710 #Bogislaw I
	}
	1187.3.18 = {
		 221712 #Bogislaw II
	}
	1220.4.1 = {
		 451953 #Barnim I
	}
	1278.2.14 = {
		 471660 #Bogislaw
	}
	1294.1.1 = {
		 451950 #Otton
	}
	
	
	
	

}

